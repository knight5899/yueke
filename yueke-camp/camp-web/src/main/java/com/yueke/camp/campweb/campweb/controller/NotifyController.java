package com.yueke.camp.campweb.campweb.controller;

import com.alipay.api.internal.util.AlipaySignature;
import com.yueke.camp.campweb.campweb.config.AlipayConfig;
import com.yueke.common.core.domain.camp.YuekeRvOrders;
import com.yueke.common.core.dto.ResultDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
@RequestMapping("/camp")
public class NotifyController {
    @RequestMapping("/rv/returnUrl")
    @ResponseBody
    public ResultDto returnUrl(HttpServletRequest request) throws Exception {
        ModelAndView mav = new ModelAndView();
        // 获取支付宝GET过来反馈信息（官方固定代码）
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.ALIPAY_PUBLIC_KEY, AlipayConfig.CHARSET, AlipayConfig.sign_type); // 调用SDK验证签名

        // 返回界面
        if (signVerified) {
            System.out.println("前往支付成功页面");
            return ResultDto.success().setCode(200).setMsg("支付押金成功");
        } else {
            System.out.println("前往支付失败页面");
            mav.setViewName("failReturn");
        }
        return ResultDto.success().setMsg("支付成功");
    }
    /**
     * 支付宝服务器异步通知
     *
     * @param request
     * @throws Exception
     */

    @RequestMapping("/rv/notifyUrl")
    public void notifyUrl(HttpServletRequest request) throws Exception {
        // 获取支付宝GET过来反馈信息
        System.out.println("异步回调接口");
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.ALIPAY_PUBLIC_KEY, AlipayConfig.CHARSET, AlipayConfig.sign_type); // 调用SDK验证签名

        if (signVerified) { // 验证成功 更新订单信息
            System.out.println("异步通知成功");
            // 商户订单号
            String out_trade_no = request.getParameter("out_trade_no");
            // 交易状态
            String trade_status = request.getParameter("trade_status");
            // 修改数据库,设置订单编号为已支付
            /*YuekeRvOrders yuekeRvOrders = new YuekeRvOrders();
            yuekeRvOrders.setYuekeRvOrdersId(Integer.parseInt(out_trade_no));
            yuekeRvOrders.setYuekeRvOrdersStatus(1);
            //yuekeRvOrdersService.updateById(yuekeRvOrders);*/
            System.out.println(out_trade_no);
            System.out.println(trade_status);
        } else {
            System.out.println("异步通知失败");
        }
    }

}
