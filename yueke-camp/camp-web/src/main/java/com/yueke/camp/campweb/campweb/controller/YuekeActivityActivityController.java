package com.yueke.camp.campweb.campweb.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.camp.campmodel.campdto.activity.AddCampActivity;
import com.yueke.camp.campmodel.campdto.activity.FindCampActivityParam;
import com.yueke.camp.campmodel.campdto.camp.CampParam;
import com.yueke.camp.campmodel.campdto.camp.FindCampParam;
import com.yueke.camp.campweb.campweb.service.YuekeActivityActivityService;
import com.yueke.common.core.domain.camp.YuekeActivityActivity;
import com.yueke.common.core.domain.camp.YuekeCampCamp;
import com.yueke.common.core.dto.ResultDto;
import com.yueke.common.core.exception.WoniuException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/camp")
public class YuekeActivityActivityController {
    @Autowired
    private YuekeActivityActivityService yuekeActivityActivityService;

    @PostMapping("/activity/addCamp")
    @ApiOperation(value = "新增营地活动")
    public ResultDto addCamp(@RequestBody AddCampActivity addCampActivity){
        YuekeActivityActivity yuekeActivityActivity = new YuekeActivityActivity();
        yuekeActivityActivity.setYuekeActivityStatus(0);
        BeanUtils.copyProperties(addCampActivity,yuekeActivityActivity);
        DateTime parse = DateUtil.parse(addCampActivity.getYuekeActivityStartTime(), "yyyy-MM-dd HH:mm:ss");
        DateTime parse1 = DateUtil.parse(addCampActivity.getYuekeActivityEndTime(), "yyyy-MM-dd HH:mm:ss");
        yuekeActivityActivity.setYuekeActivityStartTime(parse);
        yuekeActivityActivity.setYuekeActivityEndTime(parse1);
        yuekeActivityActivityService.save(yuekeActivityActivity);
        return ResultDto.success().setCode(200).setMsg("新增营地活动成功");
    }

    /**
     * 营地活动上线
     * @param yuekeActivityId
     * @return
     */
    @GetMapping("/activity/updateActivityStatusToUp")
    @ApiOperation(value = "营地活动上线")
    public ResultDto campActivityToUp(Integer yuekeActivityId){
        System.out.println(yuekeActivityId);
        if("".equals(yuekeActivityId)||yuekeActivityId==null){
            throw new WoniuException(3002,"参数异常");
        }else {
            FindCampActivityParam findCampActivityParam = new FindCampActivityParam();
            findCampActivityParam.setYuekeActivityId(yuekeActivityId);
            findCampActivityParam.setYuekeActivityStatus(0);
           ResultDto camp = this.findCamp(findCampActivityParam);
            if(camp.getCode()==200){

                YuekeActivityActivity yuekeActivityActivity = new YuekeActivityActivity();
                yuekeActivityActivity.setYuekeActivityId(yuekeActivityId);
                yuekeActivityActivity.setYuekeActivityStatus(1);
                yuekeActivityActivityService.updateById(yuekeActivityActivity);
                return ResultDto.success().setCode(200).setMsg("上线成功");
            }else{
                return ResultDto.success().setCode(500).setMsg("查询为空");
            }

        }
    }

    /**
     * 条件分页查询营地活动
     * @param findCampActivityParam
     * @return
     */
    @GetMapping("/activity/findCampActivity")
    @ApiOperation(value="条件分页查询营地活动")
    public ResultDto findCamp(FindCampActivityParam findCampActivityParam){
        System.out.println(findCampActivityParam.toString());
        QueryWrapper<YuekeActivityActivity> wrapper = new QueryWrapper<>();
        if(findCampActivityParam.getYuekeActivityCampId()!=null){
            wrapper.eq("yueke_activity_camp_id",findCampActivityParam.getYuekeActivityCampId());
        }

        if(findCampActivityParam.getYuekeActivityId()!=null){
            wrapper.eq("yueke_activity_id",findCampActivityParam.getYuekeActivityId());
        }
        if(findCampActivityParam.getYuekeActivityStatus()!=null){
            wrapper.eq("yueke_activity_status",findCampActivityParam.getYuekeActivityStatus());
        }
        if(findCampActivityParam.getYuekeActivityTittle()!=null){
            wrapper.like("yueke_activity_tittle",findCampActivityParam.getYuekeActivityTittle());
        }
        if(findCampActivityParam.getYuekeActivityStartTimeContition()!=null&&findCampActivityParam.getYuekeActivityEndTimeContion()!=null){
            DateTime parse = DateUtil.parse(findCampActivityParam.getYuekeActivityStartTimeContition(), "yyyy-MM-dd HH:mm:ss");
            DateTime parse1 = DateUtil.parse(findCampActivityParam.getYuekeActivityEndTimeContion(), "yyyy-MM-dd HH:mm:ss");

            wrapper.between("yueke_activity_start_time",parse,parse1);
        }
        Page<YuekeActivityActivity> yuekeCampCampPage = new Page<>(findCampActivityParam.getPageNumber(), findCampActivityParam.getPageSize());
        Page<YuekeActivityActivity> page = yuekeActivityActivityService.page(yuekeCampCampPage, wrapper);
        System.out.println(page.getRecords());
        if(page.getRecords().isEmpty()){
            return ResultDto.success().setCode(500).setMsg("查询为空");
        }
        return ResultDto.success().setCode(200).setMsg("查询成功").setData(page);
    }
    /**
     * 营地活动下线
     * @param yuekeActivityId
     * @return
     */
    @GetMapping("/activity/updateActivityStatusToDown")
    @ApiOperation(value = "营地活动上线")
    public ResultDto campActivityToDown(Integer yuekeActivityId){
        System.out.println(yuekeActivityId);
        if("".equals(yuekeActivityId)||yuekeActivityId==null){
            throw new WoniuException(3002,"参数异常");
        }else {
            FindCampActivityParam findCampActivityParam = new FindCampActivityParam();
            findCampActivityParam.setYuekeActivityId(yuekeActivityId);
            findCampActivityParam.setYuekeActivityStatus(1);
            ResultDto camp = this.findCamp(findCampActivityParam);
            if(camp.getCode()==200){

                YuekeActivityActivity yuekeActivityActivity = new YuekeActivityActivity();
                yuekeActivityActivity.setYuekeActivityId(yuekeActivityId);
                yuekeActivityActivity.setYuekeActivityStatus(0);
                yuekeActivityActivityService.updateById(yuekeActivityActivity);
                return ResultDto.success().setCode(200).setMsg("下线成功");
            }else{
                return ResultDto.success().setCode(500).setMsg("查询为空");
            }
        }
    }





}
