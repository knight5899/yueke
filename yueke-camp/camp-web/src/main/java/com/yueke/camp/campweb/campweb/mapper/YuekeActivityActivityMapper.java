package com.yueke.camp.campweb.campweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.camp.YuekeActivityActivity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekeActivityActivityMapper extends BaseMapper<YuekeActivityActivity> {

}
