package com.yueke.camp.campweb.campweb.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.camp.campmodel.campdto.rv.AddRvOrdersParam;
import com.yueke.common.core.domain.camp.YuekeRvOrders;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekeRvOrdersService extends IService<YuekeRvOrders> {

    void addOrders(AddRvOrdersParam addRvOrdersParam);

    void payDeposityRecord(String yuekeRvOrdersUuid);
}
