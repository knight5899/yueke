package com.yueke.camp.campweb.campweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.camp.campweb.campweb.mapper.YuekeCampCampMapper;
import com.yueke.camp.campweb.campweb.service.YuekeCampCampService;
import com.yueke.common.core.domain.camp.YuekeCampCamp;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekeCampCampServiceImpl extends ServiceImpl<YuekeCampCampMapper, YuekeCampCamp> implements YuekeCampCampService {

}
