package com.yueke.camp.campweb.campweb.service.impl;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.camp.campmodel.campdto.rv.AddRvOrdersParam;
import com.yueke.camp.campweb.campweb.config.AlipayConfig;
import com.yueke.camp.campweb.campweb.mapper.YuekeRvMapper;
import com.yueke.camp.campweb.campweb.mapper.YuekeRvOrdersMapper;
import com.yueke.camp.campweb.campweb.service.YuekeRvOrdersService;
import com.yueke.camp.campweb.campweb.service.YuekeRvService;
import com.yueke.common.core.domain.camp.YuekeRv;
import com.yueke.common.core.domain.camp.YuekeRvOrders;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekeRvOrdersServiceImpl extends ServiceImpl<YuekeRvOrdersMapper, YuekeRvOrders> implements YuekeRvOrdersService {
    @Autowired
    private YuekeRvOrdersMapper yuekeRvOrdersMapper;
    @Autowired
    private YuekeRvService yuekeRvService;

    @Override
    public void addOrders(AddRvOrdersParam addRvOrdersParam) {
        String replace = UUID.randomUUID().toString().replace("-", "");
        YuekeRvOrders yuekeRvOrders = new YuekeRvOrders();
        yuekeRvOrders.setYuekeRvOrdersUuid(replace);
        BeanUtils.copyProperties(addRvOrdersParam,yuekeRvOrders);
        DateTime parse = DateUtil.parse(addRvOrdersParam.getYuekeRvOrdersArriveTime(), "yyyy-MM-dd HH:mm:ss");
        //订单生成时间
        DateTime parse1 = DateUtil.parse(DateUtil.now(), "yyyy-MM-dd HH:mm:ss");
        yuekeRvOrders.setYuekeRvOrdersCreatetime(parse1);
        yuekeRvOrders.setYuekeRvOrdersArriveTime(parse);
        yuekeRvOrders.setYuekeRvOrdersStatus(0);
        yuekeRvOrders.setYuekeRvOrdersId(0);
        yuekeRvOrders.setYuekeRvOrdersPaystatus(0);
        yuekeRvOrders.setYuekeRvOrdersDepositStatus(0);
        yuekeRvOrdersMapper.insert(yuekeRvOrders);
    }

    /**
     * 押金支付
     * @param yuekeRvOrdersUuid
     */
    @Override
    public void payDeposityRecord(String yuekeRvOrdersUuid) {
        QueryWrapper<YuekeRvOrders> objectQueryWrapper = new QueryWrapper<>();
        YuekeRvOrders yuekeRvOrders = yuekeRvOrdersMapper.selectOne(objectQueryWrapper.eq("yueke_rv_orders_uuid", yuekeRvOrdersUuid));
        YuekeRv byId = yuekeRvService.getById(yuekeRvOrders.getYuekeRvOrdersRvId());
        BigDecimal yuekeRvDepositMon = byId.getYuekeRvDepositMon();


        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.APP_ID, AlipayConfig.APP_PRIVATE_KEY, "json", AlipayConfig.CHARSET, AlipayConfig.ALIPAY_PUBLIC_KEY, AlipayConfig.sign_type);
        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填

        String ordersId = yuekeRvOrdersUuid;
        // String out_trade_no = request.getParameter("Order");
        //付款金额，必填  ShopName
        BigDecimal totalprice = yuekeRvDepositMon;
        //String total_amount = request.getParameter("Money");
        //订单名称，必填
        String name = "房车押金支付";
        //String subject = request.getParameter("Name");
        //商品描述，可空
        String body = "房车押金订单测试";
        System.out.println(ordersId+":"+totalprice+":"+name+":"+body);
        // String body =request.getParameter("购物测试");
        // 该笔订单允许的最晚付款时间，逾期将关闭交易。取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。
        String timeout_express = "1c";
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + ordersId + "\","
                + "\"total_amount\":\"" + totalprice + "\","
                + "\"subject\":\"" + name + "\","
                + "\"body\":\"" + body + "\","
                + "\"timeout_express\":\""+ timeout_express +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        //请求
        String url = "";
        try {
            url = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            System.out.println(url);
               /* response.setContentType("text/html;charset=" + AlipayConfig.CHARSET);

                response.getWriter().write(url); // 直接将完整的表单html输出到页面
                response.getWriter().flush();
                response.getWriter().close();*/

        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        System.out.println(url);
    }
}
