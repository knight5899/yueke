package com.yueke.camp.campweb.campweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.common.core.domain.camp.YuekeCampCamp;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekeCampCampMapper extends BaseMapper<YuekeCampCamp> {
//    IPage<YuekeCampCamp> selectPageCamp(Page<YuekeCampCamp> page,);
}
