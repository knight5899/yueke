package com.yueke.camp.campweb.campweb.config;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号,开发时使用沙箱提供的APPID，生产环境改成自己的APPID
    public static String APP_ID = "2021000116661066"; //测试

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCIxCD+IY2FYDk9hLECXTF9Y2d4negSjddfbZkffwnALpiJ3jt8QzlzK8SnNnQ/7Dp7LUfEy2MJbVw+IkdD3VnrYKbE+F3pk8OZz9hOQH/RnQ3ji+RggWHz1XZL9VF4G3JBU6nogmCHMlGbwytwW6rswEYt/DVaMqvK3eSJnZetKCOvpR129mUQGLu1cQA/zasDXHc4NVYwu0j/qGSMfXEb4USH5JliaXzTUn6sEhVZ7B0I6ygnfqZG5F8d6NkTglEg30eyxuxhdhI4bMPZkLUbuuSsiZsk1VbBqJ1M5ILq541kZ5tIRh5ajmFct/YDlXJfmpYnMo79Tklo9PPdySpfAgMBAAECggEAaVpL3/eQwNFixpTUlfGcEe7k4fEhuEkzQvt2HOELbdN3BZz/TbWrJ6Drl7NG0YbzdbdntZzG6ZgommRlaU212q9VSB3amfeNkYqeIYll7N6CuGomvoBsKByjR9ftrgvWc6QzGjx0hzCn1Mj/SUoT3NQwO3Qw1Q57b4aGacvP7z32tIuVFugZkS9wW2Aa0705FyU+PL9Oyor639K3gNypJacGK5dsTBuLPh/2llvGxDJbPnVHho+67bH+1M5QDE2D8gluyEGwkdZmDMNV4r8Yq4aQp8qJwz6xfVo4dNbsNBNqt3E+Z0ykVIf7eiJ6vSEPm9lF/KY3noElmeabgQJTwQKBgQDxWefIGzkJBE0zT0/pq9oGJHNxY5+a4zWeLviEe/4MxPogq1P903kbXZClPuODzX2/+IKc8phoY6x50ih7JEAkCncEfxhYZDHVCubHmHyDIwjznyGVGF1vQCJOKMCvzGQfecEZRpuLmyA9gSXw20SvJmhKPrtQ4F5X3PPPqr4fZwKBgQCRETCEGXy7whRDHbGorpBKXPtKlPevCsepvwg7+MkC7PXjH4iOO//7bLwqkdKOYmr/0OXJCDKu69BdgL8vp4Hdk++SE5IOGWzttfG3/TmfGsFWAy/+BxITn2docI02mGZZRgx8FLetqIIUMB9+hD30tf9Qe4A/SSwLISD6yDhaSQKBgQDNVLvIlsBzV2GGoZdPKZGXYDOG6EbFM0BOj4+GxyZgHsTePmR6sgt6qKOCsnpFi/HL/NTCSfI3/XSjP5SJD1IyV2TiQm6n5LYwldG5RZXIy2m2OlAL1GrJVzrYW/tylOUpkiAiLNvwbc2EBfgNbb6FdJuNbslWCUy6W5RhvNV7SQKBgHdj70r48vhQDX8DREDwkh4xkjtaiNJOC+8/IpbA9SZsQR4QyMFOzilt3xsl8mVuyGML2OHv1k6yE0Ww/ShKMw5af3P4mV7zO/wTnwFu7eptCasxamEmDpGJwoBpCT9ig/F7PjEsl9LbeVsGFBV63sa0tpDVY8IyBYaPWGYtPHvJAoGBALYdCsx4sEI4IN5V5lMeEn3bubyLlOI6tF94zRSjZxf7ZGDK4jjO4qqUT2CYQXzVoO6zQlRPX5xP9L5W9Ri4VyhyoytFQxVFibx+iHEHlEA7aEpEOL8bqtAJSDL0vnorMzvZMfZ9ED07GmvARZSxQLGmB+Tq4TGeOWSXlTKIdbxK";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnx7nksCGqvXdPzBdbVjNnuTm2u2rPBbug+PJg4ztlxqU2K9ItI8fi4nf1AXD/k0i8U/Y/HZ/A5pvEgrW+SnPF+J4GL5sd6yKUvcedGX/IwkQ28chn66e3jNEHYbbWIRsA5O9blSmpehNVxa2DphoU/hbagxm26lMiSqauNhCZXC/039PjLPZlPWag8VQ9FLYpwYbzNCabVwF/M8fbFbKXay5dJxHE9onUJbV+6EFtj3VxHIFYQ4PJqF3jY9qUiY4parTOrHLeUs+MdpniE52TtdhS5Dmhj7w2uDKZifmka08fGpyiuXoCrSEsvEXv1mwX/6HE6yLGPBv2slurqagZwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://330561t4i3.zicp.vip/camp/rv/notifyUrl";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问(其实就是支付成功后返回的页面)
    public static String return_url = "http://330561t4i3.zicp.vip/camp/rv/returnUrl";
    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String CHARSET = "UTF-8";

    // 支付宝网关，这是沙箱的网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do"; //测试

    // 支付宝网关
    public static String log_path = "E:\\";

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis() + ".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
