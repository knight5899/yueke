package com.yueke.camp.campweb.campweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.camp.campweb.campweb.mapper.YuekeRvOrdersItemMapper;
import com.yueke.camp.campweb.campweb.service.YuekeRvOrdersItemService;
import com.yueke.common.core.domain.camp.YuekeRvOrdersItem;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekeRvOrdersItemServiceImpl extends ServiceImpl<YuekeRvOrdersItemMapper, YuekeRvOrdersItem> implements YuekeRvOrdersItemService {

}
