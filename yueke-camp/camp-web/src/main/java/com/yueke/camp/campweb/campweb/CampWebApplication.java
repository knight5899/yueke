package com.yueke.camp.campweb.campweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients
@ComponentScan("com.yueke.yuekecommon.yuekeweb.config")
@ComponentScan("com.yueke.camp.campweb.campweb.controller")
public class CampWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(CampWebApplication.class, args);
    }

}
