package com.yueke.camp.campweb.campweb.controller;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.camp.campmodel.campdto.camp.CampParam;
import com.yueke.camp.campmodel.campdto.camp.FindCampParam;
import com.yueke.camp.campmodel.campdto.camp.UpdateCampParem;
import com.yueke.camp.campweb.campweb.mapper.YuekeCampCampMapper;
import com.yueke.camp.campweb.campweb.service.YuekeCampCampService;
import com.yueke.common.core.domain.camp.YuekeCampCamp;
import com.yueke.common.core.dto.ResultDto;
import com.yueke.common.core.exception.WoniuException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("camp")
public class YuekeCampCampController {
    @Autowired
    private YuekeCampCampService yuekeCampCampService;
    @Autowired
    private YuekeCampCampMapper yuekeCampCampMapper;
    /**
     * 新增营地
     * @param campParam
     * @return
     */
    @PostMapping("/camp/addCamp")
    @ApiOperation(value = "新增营地")
    public ResultDto addCamp(@RequestBody CampParam campParam){
        YuekeCampCamp yuekeCampCamp = new YuekeCampCamp();
        yuekeCampCamp.setYuekeCampUpDownStatus(0);
        BeanUtils.copyProperties(campParam,yuekeCampCamp);
        yuekeCampCampService.save(yuekeCampCamp);
        return ResultDto.success().setCode(200).setMsg("新增营地成功");
    }

    /**
     * 条件分页查询营地
     * @param findCampParam
     * @return
     */
    @GetMapping("/camp/findCamp")
    @ApiOperation(value="条件分页查询营地")
    public ResultDto findCamp(FindCampParam findCampParam){
        System.out.println(findCampParam.toString());
        QueryWrapper<YuekeCampCamp> wrapper = new QueryWrapper<>();
        if(findCampParam.getYuekeCampId()!=null){
            wrapper.eq("yueke_camp_id",findCampParam.getYuekeCampId());
        }

        if(findCampParam.getYuekeCampName()!=null){
            wrapper.like("yueke_camp_name",findCampParam.getYuekeCampName());
        }
        if(findCampParam.getYuekeCampUpDownStatus()!=null){
            wrapper.eq("yueke_camp_up_down_status",findCampParam.getYuekeCampUpDownStatus());
        }
        Page<YuekeCampCamp> yuekeCampCampPage = new Page<>(findCampParam.getPageNumber(), findCampParam.getPageSize());
        Page<YuekeCampCamp> yuekeCampCampPage1 = yuekeCampCampMapper.selectPage(yuekeCampCampPage, wrapper);
        System.out.println(yuekeCampCampPage1.getRecords());
        if(yuekeCampCampPage1.getRecords().isEmpty()){
            return ResultDto.success().setCode(500).setMsg("查询为空");
        }
        return ResultDto.success().setCode(200).setMsg("查询成功").setData(yuekeCampCampPage1);
    }

    /**
     * 营地上线
     * @param yuekeCampId
     * @return
     */
    @GetMapping("/camp/updateCampStatustoUp")
    @ApiOperation(value = "营地上线")
    public ResultDto campToUp(Integer yuekeCampId){
        System.out.println(yuekeCampId);
        if("".equals(yuekeCampId)||yuekeCampId==null){
            throw new WoniuException(3002,"参数异常");
        }else {
            FindCampParam findCampParam = new FindCampParam();
            findCampParam.setYuekeCampId(yuekeCampId);
            findCampParam.setYuekeCampUpDownStatus(0);
            ResultDto camp = this.findCamp(findCampParam);
            if(camp.getCode()==200){
                YuekeCampCamp yuekeCampCamp = new YuekeCampCamp();
                yuekeCampCamp.setYuekeCampId(yuekeCampId);
                yuekeCampCamp.setYuekeCampUpDownStatus(1);
                //上线时间
                String upTime = DateUtil.formatDateTime(new Date());
                DateTime parse = DateUtil.parse(upTime, "yyyy-MM-dd HH:mm:ss");
                yuekeCampCamp.setYuekeCampUpTime(parse);
                //置下线时间为空
                yuekeCampCampService.updateById(yuekeCampCamp);
                return ResultDto.success().setCode(200).setMsg("上线成功");
            }else{
                return ResultDto.success().setCode(500).setMsg("查询为空");
            }
        }
    }
    /**
     * 营地下线
     * @param yuekeCampId
     * @return
     */
    @GetMapping("/camp/updateCampStatustoDown")
    @ApiOperation(value = "营地下线")
    public ResultDto campToDown(Integer yuekeCampId){
        System.out.println(yuekeCampId);
        if("".equals(yuekeCampId)||yuekeCampId==null){
            throw new WoniuException(3002,"参数异常");
        }else {
            FindCampParam findCampParam = new FindCampParam();
            findCampParam.setYuekeCampId(yuekeCampId);
            findCampParam.setYuekeCampUpDownStatus(1);
            ResultDto camp = this.findCamp(findCampParam);
            if(camp.getCode()==200){
                YuekeCampCamp yuekeCampCamp = new YuekeCampCamp();
                yuekeCampCamp.setYuekeCampId(yuekeCampId);
                yuekeCampCamp.setYuekeCampUpDownStatus(0);
                //下线时间
                String upTime = DateUtil.formatDateTime(new Date());
                DateTime parse = DateUtil.parse(upTime, "yyyy-MM-dd HH:mm:ss");
                yuekeCampCamp.setYuekeCampDownlineTime(parse);
                yuekeCampCamp.setYuekeCampUpTime(null);
                yuekeCampCampService.updateById(yuekeCampCamp);
                return ResultDto.success().setCode(200).setMsg("下线成功");
            }else{
                return ResultDto.success().setCode(500).setMsg("查询为空或已下线");
            }
        }
    }

    /**
     * 修改营地资料
     * @param updateCampParem
     * @return
     */
    @PostMapping("/camp/updateCamp")
    @ApiOperation(value = "修改营地资料")
    public ResultDto updateCamp(@RequestBody UpdateCampParem updateCampParem){
        System.out.println(updateCampParem.toString());
        FindCampParam findCampParam = new FindCampParam();
        findCampParam.setYuekeCampId(updateCampParem.getYuekeCampId());
        ResultDto camp = this.findCamp(findCampParam);
        if(camp.getCode()==200){
            YuekeCampCamp yuekeCampCamp = new YuekeCampCamp();
            BeanUtils.copyProperties(updateCampParem,yuekeCampCamp);
            if(updateCampParem.getYuekeCampDownlineTime()!=null){
                DateTime downTime = DateUtil.parse(updateCampParem.getYuekeCampDownlineTime(), "yyyy-MM-dd HH:mm:ss");
                yuekeCampCamp.setYuekeCampDownlineTime(downTime);
            }
            if(updateCampParem.getYuekeCampUpTime()!=null){
                DateTime upTime = DateUtil.parse(updateCampParem.getYuekeCampUpTime(), "yyyy-MM-dd HH:mm:ss");
                yuekeCampCamp.setYuekeCampUpTime(upTime);
            }
            yuekeCampCampService.updateById(yuekeCampCamp);
            return ResultDto.success().setCode(200).setMsg("修改营地资料成功");
        }else {
            return ResultDto.success().setCode(500).setMsg("该营地不存在");
        }
    }

}

