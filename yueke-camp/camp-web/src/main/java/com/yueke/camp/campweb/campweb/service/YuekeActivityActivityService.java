package com.yueke.camp.campweb.campweb.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.camp.YuekeActivityActivity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekeActivityActivityService extends IService<YuekeActivityActivity> {

}
