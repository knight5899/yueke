package com.yueke.camp.campweb.campweb.service.impl;


import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.camp.campmodel.campdto.activity.AddCampActivityOrdersParam;
import com.yueke.camp.campweb.campweb.mapper.YuekeActivityOrdersMapper;
import com.yueke.camp.campweb.campweb.service.YuekeActivityActivityService;
import com.yueke.camp.campweb.campweb.service.YuekeActivityOrdersService;
import com.yueke.common.core.domain.camp.YuekeActivityActivity;
import com.yueke.common.core.domain.camp.YuekeActivityOrders;
import com.yueke.common.core.exception.WoniuException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekeActivityOrdersServiceImpl extends ServiceImpl<YuekeActivityOrdersMapper, YuekeActivityOrders> implements YuekeActivityOrdersService {
    @Autowired
    private YuekeActivityOrdersMapper yuekeActivityOrdersMapper;
    @Autowired
    private YuekeActivityActivityService yuekeActivityActivityService;
    @Override
    public void addActivityOrders(AddCampActivityOrdersParam addCampActivityOrdersParam) {
        Integer yuekeActivityId = addCampActivityOrdersParam.getYuekeActivityId();
        YuekeActivityActivity byId = yuekeActivityActivityService.getById(yuekeActivityId);

        if(byId!=null){
            BigDecimal yuekeActivityTicketPrice = byId.getYuekeActivityTicketPrice();
            Integer yuekeActivityTicketNum = addCampActivityOrdersParam.getYuekeActivityTicketNum();
            //计算总价
            BigDecimal bigDecimal = new BigDecimal(yuekeActivityTicketNum.toString());
            BigDecimal totalPrice = bigDecimal.multiply(yuekeActivityTicketPrice);
            YuekeActivityOrders yuekeActivityOrders = new YuekeActivityOrders();
            BeanUtils.copyProperties(addCampActivityOrdersParam,yuekeActivityOrders);
            yuekeActivityOrders.setYuekeActivityTicketTotalMoney(totalPrice);
            //下单时间
            yuekeActivityOrders.setYuekeActivityOrdersCreateTime(DateUtil.parse(DateUtil.now(),"yyyy-MM-dd HH:mm:ss"));
            yuekeActivityOrders.setYuekeActivityTicketPrice(byId.getYuekeActivityTicketPrice());
            yuekeActivityOrders.setYuekeActivityOrdersPaystatus(0);
            yuekeActivityOrdersMapper.insert(yuekeActivityOrders);
        }else{
            throw new WoniuException(500,"该活动不存在");
        }




    }
}
