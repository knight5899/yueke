package com.yueke.camp.campweb.campweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.camp.campweb.campweb.mapper.YuekeActivityActivityMapper;
import com.yueke.camp.campweb.campweb.service.YuekeActivityActivityService;
import com.yueke.common.core.domain.camp.YuekeActivityActivity;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekeActivityActivityServiceImpl extends ServiceImpl<YuekeActivityActivityMapper, YuekeActivityActivity> implements YuekeActivityActivityService {

}
