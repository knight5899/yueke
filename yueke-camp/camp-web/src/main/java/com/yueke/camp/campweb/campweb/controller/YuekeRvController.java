package com.yueke.camp.campweb.campweb.controller;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.camp.campmodel.campdto.camp.CampParam;
import com.yueke.camp.campmodel.campdto.camp.FindCampParam;
import com.yueke.camp.campmodel.campdto.rv.AddRvParam;
import com.yueke.camp.campmodel.campdto.rv.FindRvConditionParam;
import com.yueke.camp.campweb.campweb.mapper.YuekeRvMapper;
import com.yueke.camp.campweb.campweb.service.YuekeRvService;
import com.yueke.common.core.domain.camp.YuekeCampCamp;
import com.yueke.common.core.domain.camp.YuekeRv;
import com.yueke.common.core.dto.ResultDto;
import com.yueke.common.core.exception.WoniuException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("/camp")
public class YuekeRvController {
    @Autowired
    private YuekeRvService yuekeRvService;
    @Autowired
    private YuekeRvMapper yuekeRvMapper;
    /**
     * 新增营地
     * @param addRvParam
     * @return
     */
    @PostMapping("/rv/addRv")
    @ApiOperation(value = "新增营地")
    public ResultDto addRv(@RequestBody AddRvParam addRvParam){
        YuekeRv yuekeRv = new YuekeRv();
        yuekeRv.setYuekeRvStatus(0);
        BeanUtils.copyProperties(addRvParam,yuekeRv);
        yuekeRvService.save(yuekeRv);
        return ResultDto.success().setCode(200).setMsg("新增营地房车成功");
    }
    /**
     * 条件分页查询营地
     * @param findRvConditionParam
     * @return
     */
    @GetMapping("/rv/selectRVByCondition")
    @ApiOperation(value="条件分页查询房车")
    public ResultDto findCamp(FindRvConditionParam findRvConditionParam){
        System.out.println(findRvConditionParam.toString());
        QueryWrapper<YuekeRv> wrapper = new QueryWrapper<>();
        if(findRvConditionParam.getYuekeRvId()!=null){
            wrapper.eq("yueke_rv_id",findRvConditionParam.getYuekeRvId());
        }
        if(findRvConditionParam.getYuekeRvStatus()!=null){
            wrapper.eq("yueke_rv_status",findRvConditionParam.getYuekeRvStatus());
        }
        if(findRvConditionParam.getYuekeCampName()!=null){
            wrapper.like("yueke_camp_name",findRvConditionParam.getYuekeCampName());
        }
        if(findRvConditionParam.getYuekeRvType()!=null){
            wrapper.eq("yueke_rv_type",findRvConditionParam.getYuekeRvType());
        }
        if(findRvConditionParam.getYuekeRvMinPrice()!=null&&findRvConditionParam.getYuekeRvMaxPrice()!=null){
            wrapper.between("yueke_rv_price",findRvConditionParam.getYuekeRvMinPrice(),findRvConditionParam.getYuekeRvMaxPrice());
        }
        Page<YuekeRv> yuekeCampRvPage = new Page<>(findRvConditionParam.getPageNumber(), findRvConditionParam.getPageSize());
        Page<YuekeRv> yuekeCampRvPage1 = yuekeRvMapper.selectPage(yuekeCampRvPage, wrapper);
        System.out.println(yuekeCampRvPage1.getRecords());
        if(yuekeCampRvPage1.getRecords().isEmpty()){
            return ResultDto.success().setCode(500).setMsg("查询为空");
        }
        return ResultDto.success().setCode(200).setMsg("查询成功").setData(yuekeCampRvPage1);
    }
    /**
     * 房车上线
     * @param yuekeRvId
     * @return
     */
    @GetMapping("/rv/updateRVStatustoUp")
    @ApiOperation(value = "房车上线")
    public ResultDto rvToUp(Integer yuekeRvId){
        System.out.println(yuekeRvId);
        if("".equals(yuekeRvId)||yuekeRvId==null){
            throw new WoniuException(3002,"参数异常");
        }else {
            FindRvConditionParam findRvConditionParam = new FindRvConditionParam();
            findRvConditionParam.setYuekeRvId(yuekeRvId);
            findRvConditionParam.setYuekeRvStatus(0);
            ResultDto camp = this.findCamp(findRvConditionParam);
            if(camp.getCode()==200){
                YuekeRv yuekeRv = new YuekeRv();
                yuekeRv.setYuekeRvId(yuekeRvId);
                yuekeRv.setYuekeRvStatus(1);
                //上线时间
                String upTime = DateUtil.formatDateTime(new Date());
                DateTime parse = DateUtil.parse(upTime, "yyyy-MM-dd HH:mm:ss");
                yuekeRv.setYuekeRvCreatetime(parse);
                //置下线时间为空
                yuekeRvService.updateById(yuekeRv);
                return ResultDto.success().setCode(200).setMsg("上线成功");
            }else{
                return ResultDto.success().setCode(500).setMsg("查询为空");
            }
        }
    }
    /**
     * 房车下线
     * @param yuekeRvId
     * @return
     */
    @GetMapping("/rv/updateRVStatustoDown")
    @ApiOperation(value = "房车下线")
    public ResultDto rvToDown(Integer yuekeRvId){
        System.out.println(yuekeRvId);
        if("".equals(yuekeRvId)||yuekeRvId==null){
            throw new WoniuException(3002,"参数异常");
        }else {
            FindRvConditionParam findRvConditionParam = new FindRvConditionParam();
            findRvConditionParam.setYuekeRvId(yuekeRvId);
            findRvConditionParam.setYuekeRvStatus(1);
            ResultDto camp = this.findCamp(findRvConditionParam);
            if(camp.getCode()==200){
                YuekeRv yuekeRv = new YuekeRv();
                yuekeRv.setYuekeRvId(yuekeRvId);
                yuekeRv.setYuekeRvStatus(0);
                //下线时间
                String upTime = DateUtil.formatDateTime(new Date());
                DateTime parse = DateUtil.parse(upTime, "yyyy-MM-dd HH:mm:ss");
                yuekeRv.setYuekeRvDowntime(parse);
                //置下线时间为空
                yuekeRvService.updateById(yuekeRv);
                return ResultDto.success().setCode(200).setMsg("下线成功");
            }else{
                return ResultDto.success().setCode(500).setMsg("查询为空");
            }
        }
    }


}

