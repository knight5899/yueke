package com.yueke.camp.campweb.campweb.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.camp.campmodel.campdto.activity.AddCampActivityOrdersParam;
import com.yueke.common.core.domain.camp.YuekeActivityOrders;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekeActivityOrdersService extends IService<YuekeActivityOrders> {

    void addActivityOrders(AddCampActivityOrdersParam addCampActivityOrdersParam);
}
