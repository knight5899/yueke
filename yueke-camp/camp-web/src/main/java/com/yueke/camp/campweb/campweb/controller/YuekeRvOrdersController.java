package com.yueke.camp.campweb.campweb.controller;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.camp.campmodel.campdto.rv.AddRvOrdersParam;
import com.yueke.camp.campmodel.campdto.rv.FindRvOrdersConditionParam;
import com.yueke.camp.campmodel.campdto.rv.PayRvOrdersParam;
import com.yueke.camp.campweb.campweb.config.AlipayConfig;
import com.yueke.camp.campweb.campweb.mapper.YuekeRvOrdersMapper;
import com.yueke.camp.campweb.campweb.service.YuekeRvOrdersService;
import com.yueke.common.core.domain.camp.YuekeCampCamp;
import com.yueke.common.core.domain.camp.YuekeRvOrders;
import com.yueke.common.core.dto.ResultDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("/camp")
public class YuekeRvOrdersController {
    @Autowired
    private YuekeRvOrdersService yuekeRvOrdersService;


    @PostMapping("/rv/addRVOrders")
    @ApiOperation(value = "生成订单")
    public ResultDto addRvOrders(@RequestBody AddRvOrdersParam addRvOrdersParam){
        yuekeRvOrdersService.addOrders(addRvOrdersParam);
        return ResultDto.success().setCode(200).setMsg("下单成功");
    }

    /**
     * 取消房车订单
     * @param yuekeRvOrdersUserId
     * @return
     */
    @GetMapping("/rv/deleteRVOrders")
    @ApiOperation(value = "取消订单")
    public ResultDto deleteRvOrders(Integer yuekeRvOrdersUserId){
        System.out.println(yuekeRvOrdersUserId);
        YuekeRvOrders yuekeRvOrders = new YuekeRvOrders();
        yuekeRvOrders.setYuekeRvOrdersStatus(2);
        boolean update = yuekeRvOrdersService.update(yuekeRvOrders, new UpdateWrapper<YuekeRvOrders>().eq("yueke_rv_orders_id", yuekeRvOrdersUserId));
        if(update){
            return ResultDto.success().setCode(200).setMsg("取消订单成功");
        }
        return ResultDto.success().setCode(500).setMsg("取消订单失败");
    }

    /**
     * 条件查询订单列表
     * @param findRvOrdersConditionParam
     * @return
     */
    @GetMapping("/rv/selectRVOrdersByCondition")
    @ApiOperation(value = "条件查询订单列表")
    public ResultDto findRVOrdersByCondition(FindRvOrdersConditionParam findRvOrdersConditionParam){
        QueryWrapper<YuekeRvOrders> queryWrapper = new QueryWrapper<>();
        if(findRvOrdersConditionParam.getYuekeCampId()!=null){
            queryWrapper.eq("yueke_camp_id",findRvOrdersConditionParam.getYuekeCampId());
         }
        if(findRvOrdersConditionParam.getYuekeRvOrdersId()!=null){
            queryWrapper.eq("yueke_rv_orders_id",findRvOrdersConditionParam.getYuekeRvOrdersId());
        }
        if(findRvOrdersConditionParam.getYuekeRvOrdersArriveEndTime()!=null&&findRvOrdersConditionParam.getYuekeRvOrdersArriveStartTime()!=null){
            DateTime arriveStartTime = DateUtil.parse(findRvOrdersConditionParam.getYuekeRvOrdersArriveStartTime(), "yyyy-MM-dd HH:mm:ss");
            DateTime arriveEndTime = DateUtil.parse(findRvOrdersConditionParam.getYuekeRvOrdersArriveEndTime(), "yyyy-MM-dd HH:mm:ss");
            queryWrapper.between("yueke_rv_orders_arrive_time",arriveStartTime,arriveEndTime);
        }
        if(findRvOrdersConditionParam.getYuekeRvOrdersDepositStatus()!=null){
            queryWrapper.eq("yueke_rv_orders_deposit_status",findRvOrdersConditionParam.getYuekeRvOrdersDepositStatus());
        }
        if(findRvOrdersConditionParam.getYuekeRvOrdersPaystatus()!=null){
            queryWrapper.eq("yueke_rv_orders_paystatus",findRvOrdersConditionParam.getYuekeRvOrdersPaystatus());
        }
        if(findRvOrdersConditionParam.getYuekeRvOrdersPhone()!=null){
            queryWrapper.eq("yueke_rv_orders_phone",findRvOrdersConditionParam.getYuekeRvOrdersPhone());
        }
        if(findRvOrdersConditionParam.getYuekeRvOrdersUserId()!=null){
            queryWrapper.eq("yueke_rv_orders_user_id",findRvOrdersConditionParam.getYuekeRvOrdersUserId());
        }
        if(findRvOrdersConditionParam.getYuekeRvOrdersUserIdentity()!=null){
            queryWrapper.eq("yueke_rv_orders_user_identity",findRvOrdersConditionParam.getYuekeRvOrdersUserIdentity());
        }
        if(findRvOrdersConditionParam.getYuekeRvOrdersStartTime()!=null&&findRvOrdersConditionParam.getYuekeRvOrdersEndTime()!=null){
            DateTime arriveStartTime = DateUtil.parse(findRvOrdersConditionParam.getYuekeRvOrdersStartTime(), "yyyy-MM-dd HH:mm:ss");
            DateTime arriveEndTime = DateUtil.parse(findRvOrdersConditionParam.getYuekeRvOrdersEndTime(), "yyyy-MM-dd HH:mm:ss");
            queryWrapper.between("yueke_rv_orders_createtime",arriveStartTime,arriveEndTime);
        }
        Page<YuekeRvOrders> yuekeRvOrdersPage = new Page<>(findRvOrdersConditionParam.getPageNumber(), findRvOrdersConditionParam.getPageSize());
        Page<YuekeRvOrders> page = yuekeRvOrdersService.page(yuekeRvOrdersPage, queryWrapper);
        if(page.getRecords().isEmpty()){
            return ResultDto.success().setCode(500).setMsg("查询为空");
        }
        return ResultDto.success().setCode(200).setMsg("查询成功").setData(page);
    }

    /**
     * 用户退房
     * @param yuekeRvOrdersId
     * @return
     */
    @GetMapping("/rv/checkOut")
    @ApiOperation(value = "用户退房")
    public ResultDto checkOutRvOrders(Integer yuekeRvOrdersId){
        YuekeRvOrders byId = yuekeRvOrdersService.getById(yuekeRvOrdersId);
        if(byId!=null){
            //退还押金
            //修改订单状态已完成
            byId.setYuekeRvOrdersStatus(1);
            yuekeRvOrdersService.updateById(byId);
        }
        return ResultDto.success().setCode(200).setMsg("退房成功");
    }

    /**
     * 订单支付
     * @param payRvOrdersParam
     * @return
     * @throws IOException
     */
    @PostMapping("/rv/payPayOrders")
    @ApiOperation(value = "订单支付")
    public String  payController (@RequestBody PayRvOrdersParam payRvOrdersParam) throws IOException {
        System.out.println("11111111111111111111111");
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.APP_ID, AlipayConfig.APP_PRIVATE_KEY, "json", AlipayConfig.CHARSET, AlipayConfig.ALIPAY_PUBLIC_KEY, AlipayConfig.sign_type);
        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填

        Integer ordersId = payRvOrdersParam.getOrdersId();
       // String out_trade_no = request.getParameter("Order");
        //付款金额，必填  ShopName
        BigDecimal totalprice = payRvOrdersParam.getTotalprice();
        //String total_amount = request.getParameter("Money");
        //订单名称，必填
        String name = payRvOrdersParam.getName();
        //String subject = request.getParameter("Name");
        //商品描述，可空
        String body = "房车订单测试";
        System.out.println(ordersId+":"+totalprice+":"+name+":"+body);
       // String body =request.getParameter("购物测试");
        // 该笔订单允许的最晚付款时间，逾期将关闭交易。取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。
        String timeout_express = "1c";
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + ordersId + "\","
                + "\"total_amount\":\"" + totalprice + "\","
                + "\"subject\":\"" + name + "\","
                + "\"body\":\"" + body + "\","
                + "\"timeout_express\":\""+ timeout_express +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        //请求
        String url = "";
        try {
            url = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            System.out.println(url);
               /* response.setContentType("text/html;charset=" + AlipayConfig.CHARSET);

                response.getWriter().write(url); // 直接将完整的表单html输出到页面
                response.getWriter().flush();
                response.getWriter().close();*/

        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        System.out.println(url);
        return url;
    }
    @GetMapping("/rv/payDeposutyRecord")
    @ApiOperation("押金支付")
    public ResultDto payDeposityRecord(String yuekeRvOrdersUuid){
        yuekeRvOrdersService.payDeposityRecord(yuekeRvOrdersUuid);
        return ResultDto.success().setCode(200).setMsg("押金支付成功");
    }

}

