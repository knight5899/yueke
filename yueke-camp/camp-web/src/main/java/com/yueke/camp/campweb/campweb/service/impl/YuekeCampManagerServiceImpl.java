package com.yueke.camp.campweb.campweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.camp.campweb.campweb.mapper.YuekeCampManagerMapper;
import com.yueke.camp.campweb.campweb.service.YuekeCampManagerService;
import com.yueke.common.core.domain.camp.YuekeCampManager;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekeCampManagerServiceImpl extends ServiceImpl<YuekeCampManagerMapper, YuekeCampManager> implements YuekeCampManagerService {

}
