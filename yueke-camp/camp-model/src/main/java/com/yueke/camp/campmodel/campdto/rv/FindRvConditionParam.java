package com.yueke.camp.campmodel.campdto.rv;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FindRvConditionParam {
    /**
     * 房车编号
     */
    private Integer yuekeRvId;
    /**
     * 房车所在营地名称
     */
    private String yuekeCampName;
    /**
     * 房车类型
     */
    private String yuekeRvType;
    /**
     * 房车入住单价
     */
    private BigDecimal yuekeRvMinPrice;
    /**
     * 房车入住单价
     */
    private BigDecimal yuekeRvMaxPrice;
    /**
     * 房车状态
     */
    private Integer yuekeRvStatus;

    /**
     * 每页展示条数
     */

    private Integer pageSize=10;
    /**
     * 第几页
     */
    private Integer pageNumber=1;


}
