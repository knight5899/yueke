package com.yueke.camp.campmodel.campdto.camp;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateCampParem {
    /**
     * 营地编号
     */
    private Integer yuekeCampId;

    /**
     * 营地名称
     */
    private String yuekeCampName;

    /**
     * 营地简介
     */
    private String yuekeCampIntroduction;

    /**
     * 营地详细介绍
     */
    private String yuekeCampDetailIntroduction;

    /**
     * 营地负责人电话
     */
    private String yuekeCampPrincipalPhone;

    /**
     * 营地联系人姓名
     */
    private String yuekeCampPrincipalName;

    /**
     * 营地所在城市
     */
    private String yuekeCampCity;



    /**
     * 营地面积
     */
    private Double yuekeCampArea;


    /**
     * 营地上线时间
     */
    private String yuekeCampUpTime;

    /**
     * 营地下线时间
     */
    private String yuekeCampDownlineTime;
    /**
     * 营地详细地址
     */
    private String yuekeCampDetailAddr;


}
