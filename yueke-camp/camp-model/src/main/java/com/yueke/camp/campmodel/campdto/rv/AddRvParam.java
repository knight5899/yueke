package com.yueke.camp.campmodel.campdto.rv;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddRvParam {
    /**
     * 投放营地id
     */
    private Integer yuekeCampId;

    /**
     * 房车所在营地名称
     */
    private String yuekeCampName;

    /**
     * 房车类型
     */
    private String yuekeRvType;

    /**
     * 房车最大入住人数
     */
    private Integer yuekeRvOccupancyMaxnum;

    /**
     * 房车所在城市
     */
    private String yuekeRvCity;

    /**
     * 房车入住每日单价
     */
    private BigDecimal yuekeRvPrice;


    /**
     * 房车详细介绍
     */
    private String yuekeRvIntroduction;

    /**
     * 房车押金金额
     */
    private BigDecimal yuekeRvDepositMon;

    /**
     * 房车图片地址
     */
    private String yuekeRvImgaddrs;
}
