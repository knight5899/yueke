package com.yueke.camp.campmodel.campdto.activity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddCampActivityOrdersParam {

    /**
     * 活动id
     */
    private Integer yuekeActivityId;

    /**
     * 活动所属营地id
     */
    private Integer yuejeActivityCampId;

    /**
     * 用户id
     */
    private Integer yuekeActivityUserId;

    /**
     * 门票购买数量
     */
    private Integer yuekeActivityTicketNum;




}
