package com.yueke.camp.campmodel.campdto.rv;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddRvOrdersParam {

    /**
     * 用户id
     */
    private Integer yuekeRvOrdersUserId;

    /**
     * 营地编号
     */
    private Integer yuekeCampId;

    /**
     * 入住总人数
     */
    private Integer yuekeRvOrdersUserNum;

    /**
     * 用户联系方式
     */
    private String yuekeRvOrdersPhone;

    /**
     * 到店时间
     */
    private String yuekeRvOrdersArriveTime;

    /**
     * 所在城市
     */
    private String yuekeRvOrdersCity;


    /**
     * 联系人身份证号码
     */
    private String yuekeRvOrdersUserIdentity;
    /**
     * 房车编号
     */
    private Integer yuekeRvOrdersRvId;

}
