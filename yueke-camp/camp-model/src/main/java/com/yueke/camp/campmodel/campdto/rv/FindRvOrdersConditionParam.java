package com.yueke.camp.campmodel.campdto.rv;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FindRvOrdersConditionParam {
    private Integer yuekeRvOrdersId;

    /**
     * 用户id
     */
    private Integer yuekeRvOrdersUserId;

    /**
     * 营地编号
     */
    private Integer yuekeCampId;

    /**
     * 用户联系方式
     */
    private String yuekeRvOrdersPhone;

    /**
     * 到店开始时间
     */
    private String yuekeRvOrdersArriveStartTime;
    /**
     * 到店结束时间
     */
    private String yuekeRvOrdersArriveEndTime;

    /**
     * 订单生成开始时间
     */
    private String yuekeRvOrdersStartTime;
    /**
     * 订单生成结束时间
     */
    private String yuekeRvOrdersEndTime;

    /**
     * 订单状态
     */
    private Integer yuekeRvOrdersStatus;

    /**
     * 订单支付状态
     */
    private Integer yuekeRvOrdersPaystatus;
    /**
     * 订单Uuid
     */
    private String yuekeRvOrdersUuid;
    /**
     * 押金状态
     */
    private Integer yuekeRvOrdersDepositStatus;

    /**
     * 联系人身份证号码
     */
    private String yuekeRvOrdersUserIdentity;
    /**
     * 每页展示条数
     */

    private Integer pageSize=10;
    /**
     * 第几页
     */
    private Integer pageNumber=1;

}
