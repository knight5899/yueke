package com.yueke.camp.campmodel.campdto.camp;



public class CampParam {
    /**
     * 营地名称
     */
    private String yuekeCampName;

    /**
     * 营地简介
     */
    private String yuekeCampIntroduction;

    /**
     * 营地详细介绍
     */
    private String yuekeCampDetailIntroduction;

    /**
     * 营地负责人电话
     */
    private String yuekeCampPrincipalPhone;

    /**
     * 营地联系人姓名
     */
    private String yuekeCampPrincipalName;

    /**
     * 营地所在城市
     */
    private String yuekeCampCity;

    /**
     * 营地详细地址
     */
    private String yuekeCampDetailAddr;

    /**
     * 营地面积
     */
    private Double yuekeCampArea;


    /**
     * 营地照片
     */
    private String yuekeCampImgaddrs;

    public String getYuekeCampName() {
        return yuekeCampName;
    }

    public void setYuekeCampName(String yuekeCampName) {
        this.yuekeCampName = yuekeCampName;
    }

    public String getYuekeCampIntroduction() {
        return yuekeCampIntroduction;
    }

    public void setYuekeCampIntroduction(String yuekeCampIntroduction) {
        this.yuekeCampIntroduction = yuekeCampIntroduction;
    }

    public String getYuekeCampDetailIntroduction() {
        return yuekeCampDetailIntroduction;
    }

    public void setYuekeCampDetailIntroduction(String yuekeCampDetailIntroduction) {
        this.yuekeCampDetailIntroduction = yuekeCampDetailIntroduction;
    }

    public String getYuekeCampPrincipalPhone() {
        return yuekeCampPrincipalPhone;
    }

    public void setYuekeCampPrincipalPhone(String yuekeCampPrincipalPhone) {
        this.yuekeCampPrincipalPhone = yuekeCampPrincipalPhone;
    }

    public String getYuekeCampPrincipalName() {
        return yuekeCampPrincipalName;
    }

    public void setYuekeCampPrincipalName(String yuekeCampPrincipalName) {
        this.yuekeCampPrincipalName = yuekeCampPrincipalName;
    }

    public String getYuekeCampCity() {
        return yuekeCampCity;
    }

    public void setYuekeCampCity(String yuekeCampCity) {
        this.yuekeCampCity = yuekeCampCity;
    }

    public String getYuekeCampDetailAddr() {
        return yuekeCampDetailAddr;
    }

    public void setYuekeCampDetailAddr(String yuekeCampDetailAddr) {
        this.yuekeCampDetailAddr = yuekeCampDetailAddr;
    }

    public Double getYuekeCampArea() {
        return yuekeCampArea;
    }

    public void setYuekeCampArea(Double yuekeCampArea) {
        this.yuekeCampArea = yuekeCampArea;
    }

    public String getYuekeCampImgaddrs() {
        return yuekeCampImgaddrs;
    }

    public void setYuekeCampImgaddrs(String yuekeCampImgaddrs) {
        this.yuekeCampImgaddrs = yuekeCampImgaddrs;
    }
}
