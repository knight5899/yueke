package com.yueke.camp.campmodel.campdto.camp;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FindCampParam {
    /**
     * 营地id
     */
    private Integer yuekeCampId;

    /**
     * 营地名称
     */
    private String yuekeCampName;

    /**
     * 营地状态
     */
    private Integer yuekeCampUpDownStatus;

    /**
     * 每页展示条数
     */

    private Integer pageSize=10;
    /**
     * 第几页
     */
    private Integer pageNumber=1;

}
