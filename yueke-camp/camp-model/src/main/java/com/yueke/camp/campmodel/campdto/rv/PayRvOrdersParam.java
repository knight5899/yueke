package com.yueke.camp.campmodel.campdto.rv;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PayRvOrdersParam {
    /**
     * 订单编号
     */
    private Integer ordersId;
    /**
     * 订单金额
     */
    private BigDecimal totalprice;
    /**
     * 订单名称
     */
    private String name;
    /**
     * 订单描述
     */
    private String productIntrotion;

}
