package com.yueke.camp.campmodel.campdto.activity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindCampActivityParam {
    private Integer yuekeActivityId;

    /**
     * 活动主题
     */
    private String yuekeActivityTittle;

       /**
     * 活动开始时间
     */
    private String yuekeActivityStartTimeContition;

    /**
     * 活动结束时间
     */
    private String yuekeActivityEndTimeContion;

       /**
     * 营地举办营地id
     */
    private Integer yuekeActivityCampId;

    /**
     * 活动状态
     */
    private Integer yuekeActivityStatus;

    /**
     * 每页展示条数
     */

    private Integer pageSize=10;
    /**
     * 第几页
     */
    private Integer pageNumber=1;


}
