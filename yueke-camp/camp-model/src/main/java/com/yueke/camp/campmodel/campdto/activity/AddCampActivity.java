package com.yueke.camp.campmodel.campdto.activity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddCampActivity {
    /**
     * 活动主题
     */
    private String yuekeActivityTittle;

    /**
     * 活动名称
     */
    private String yuekeActivityName;

    /**
     * 活动简介
     */
    private String yuekeActivityIntroduction;

    /**
     * 活动详情
     */
    private String yuekeActivityDetail;

    /**
     * 活动门票价格
     */
    private BigDecimal yuekeActivityTicketPrice;

    /**
     * 活动开始时间
     */
    private String yuekeActivityStartTime;

    /**
     * 活动结束时间
     */
    private String yuekeActivityEndTime;

    /**
     * 活动举办营地名称
     */
    private String yuekeActivityCampName;

    /**
     * 营地举办营地id
     */
    private Integer yuekeActivityCampId;



    /**
     * 活动门票总数量
     */
    private Integer yuekeActivityTicketNum;



    /**
     * 活动负责人姓名
     */
    private String yuekeActivityManagerName;

    /**
     * 活动负责人联系电话
     */
    private String yuekeActivityManagerPhone;

    /**
     * 活动图片地址
     */
    private String yuejeActivityImgaddrs;
}
