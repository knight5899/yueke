package com.yueke.camp.campmodel.campdto.activity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindActivityOrdersParam {
    private Integer yuekeActivityOrdersId;

    /**
     * 活动id
     */
    private Integer yuekeActivityId;

    /**
     * 活动所属营地id
     */
    private Integer yuejeActivityCampId;

    /**
     * 用户id
     */
    private Integer yuekeActivityUserId;


    /**
     * 订单支付状态
     */
    private Integer yuekeActivityOrdersPaystatus;
    /**
     * 每页展示条数
     */

    private Integer pageSize=10;
    /**
     * 第几页
     */
    private Integer pageNumber=1;
}
