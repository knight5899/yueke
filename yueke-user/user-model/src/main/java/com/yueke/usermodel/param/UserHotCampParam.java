package com.yueke.usermodel.param;

import lombok.Data;

/**
 * @program: yueke-parent
 * @description: 热门营地param
 * @author: XiYang
 * @create: 2020-10-17 09:32
 **/
@Data
public class UserHotCampParam {

}
