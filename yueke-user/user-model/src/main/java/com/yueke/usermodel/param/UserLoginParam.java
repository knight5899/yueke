package com.yueke.usermodel.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: yueke-parent
 * @description:
 * @author: XiYang
 * @create: 2020-10-14 09:55
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginParam {
    private String userName;
    private String passWord;
}
