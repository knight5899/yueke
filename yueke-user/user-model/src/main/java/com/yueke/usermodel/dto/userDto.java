package com.yueke.usermodel.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: yueke-parent
 * @description: userDto
 * @author: XiYang
 * @create: 2020-10-13 20:09
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class userDto {

}
