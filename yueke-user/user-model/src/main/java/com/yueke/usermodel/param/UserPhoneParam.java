package com.yueke.usermodel.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: yueke-parent
 * @description: 用户电话
 * @author: XiYang
 * @create: 2020-10-14 16:42
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPhoneParam implements Serializable {
    private String userPhone;
}
