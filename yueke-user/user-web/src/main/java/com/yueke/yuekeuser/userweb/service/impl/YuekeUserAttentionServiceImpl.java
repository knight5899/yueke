package com.yueke.yuekeuser.userweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.user.YuekeUserAttention;
import com.yueke.yuekeuser.userweb.mapper.YuekeUserAttentionMapper;
import com.yueke.yuekeuser.userweb.service.YuekeUserAttentionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Service
public class YuekeUserAttentionServiceImpl extends ServiceImpl<YuekeUserAttentionMapper, YuekeUserAttention> implements YuekeUserAttentionService {

}
