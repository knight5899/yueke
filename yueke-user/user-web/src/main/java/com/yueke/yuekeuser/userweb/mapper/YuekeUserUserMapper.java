package com.yueke.yuekeuser.userweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.user.YuekeUserUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
public interface YuekeUserUserMapper extends BaseMapper<YuekeUserUser> {

}
