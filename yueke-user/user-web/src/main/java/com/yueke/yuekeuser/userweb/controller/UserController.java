package com.yueke.yuekeuser.userweb.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yueke.common.core.domain.user.YuekeUserAttention;
import com.yueke.common.core.domain.user.YuekeUserComment;
import com.yueke.common.core.domain.user.YuekeUserReel;
import com.yueke.common.core.domain.user.YuekeUserUser;
import com.yueke.common.core.dto.ResultDto;
import com.yueke.common.core.exception.WoniuAuthencticationException;
import com.yueke.common.core.exception.WoniuExceptionCode;
import com.yueke.common.core.util.JwtUtil;
import com.yueke.yuekeuser.userweb.service.YuekeUserAttentionService;
import com.yueke.yuekeuser.userweb.service.YuekeUserCommentService;
import com.yueke.yuekeuser.userweb.service.YuekeUserReelService;
import com.yueke.yuekeuser.userweb.service.YuekeUserUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @program: yueke-parent
 * @description:
 * @author: XiYang
 * @create: 2020-10-13 17:58
 **/
@RestController
@RequestMapping("/user")
@Api(value="用户控制层UserController",tags = {"用户操作接口"})
public class UserController {

    @Autowired
    private YuekeUserAttentionService yuekeUserAttentionService;
    @Autowired
    private YuekeUserCommentService yuekeUserCommentService;
    @Autowired
    private YuekeUserReelService yuekeUserReelService;
    @Autowired
    private YuekeUserUserService yuekeUserUserService;



    /*
    @GetMapping("/user/demo")
    public ResultDto demo(){
        return ResultDto.success().setCode(200).setMsg("测试").setData("demo");
    }
    @GetMapping("/user/queyuser")
    public ResultDto queryuser(){
        List<YuekeUserUser> list = yuekeUserUserService.list(null);
        return ResultDto.success().setCode(200).setMsg("测试").setData(list);
    }
     */


    /**
     * 用户注册(根据电话号码进行注册)
     * @return
     */
    @PostMapping("/regiest")
    @ApiOperation(value = "根据电话进行用户注册")
    public ResultDto regiest(@RequestBody YuekeUserUser yuekeUserUser){
        QueryWrapper<YuekeUserUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_phone",yuekeUserUser.getUserPhone());
        YuekeUserUser one = yuekeUserUserService.getOne(queryWrapper);
        if(one!=null){
            return ResultDto.success().setCode(200).setMsg("该手机号已经存在无法注册");
        }
        boolean save = yuekeUserUserService.save(yuekeUserUser);
        return ResultDto.success().setCode(200).setMsg("注册成功").setData(save);
    }

    /**
     * 用户登录
     * @param yuekeUserUser
     * @return ResultDto
     */
    @PostMapping("/login")
    @ApiOperation(value = "根据用户名和密码进行登录")
    public ResultDto login(@RequestBody YuekeUserUser yuekeUserUser){
        QueryWrapper<YuekeUserUser> querywapper = new QueryWrapper<>();
        querywapper.eq("user_name",yuekeUserUser.getUserName()).eq("user_password",yuekeUserUser.getUserPassword());
        YuekeUserUser one = yuekeUserUserService.getOne(querywapper);
        if (one==null){
            throw new WoniuAuthencticationException(WoniuExceptionCode.USER_AUTHE_ERROR);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userInfo", one);
        map.put("loginTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        String token = JwtUtil.creatToken(map, 60 * 24);
        map.put("X-Token",token);
        return ResultDto.success().setCode(200).setMsg("登录成功").setData(map);
    }

    /**
     * 显示个人信息
     * 根据用户手机号显示个人信息
     * @return
     */
    @GetMapping("/usermsg")
    @ApiOperation(value = "根据电话显示个人信息")
    public ResultDto usermsg(@RequestBody YuekeUserUser yuekeUserUser){
        QueryWrapper<YuekeUserUser> queryWrapper = new QueryWrapper<>();
        QueryWrapper<YuekeUserUser> user_phone = queryWrapper.eq("user_phone", yuekeUserUser.getUserPhone());
        YuekeUserUser user = yuekeUserUserService.getOne(user_phone);
        if(user==null){
            return ResultDto.success().setCode(200).setMsg("显示个人信息失败").setData(user);
        }
        return ResultDto.success().setCode(5001).setMsg("个人信息").setData(user);
    }

    /**
     * 修改个人信息(根据id进行修改)
     * @return
     */
    @PostMapping("/updateusermsg")
    @ApiOperation(value = "修改个人信息")
    public ResultDto updateusermsg(@RequestBody YuekeUserUser yuekeUserUser){
        boolean update = yuekeUserUserService.updateById(yuekeUserUser);
        return ResultDto.success().setCode(200).setMsg("信息修改成功").setData(update);
    }

    /**
     * 根据用户电话注销用户
     * @param yuekeUserUser
     * @return
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除个人信息")
    public ResultDto deleteusermsg(@RequestBody YuekeUserUser yuekeUserUser){
        QueryWrapper<YuekeUserUser> yuekeUserUserQueryWrapper = new QueryWrapper<>();
        QueryWrapper<YuekeUserUser> user_phone = yuekeUserUserQueryWrapper.eq("user_phone", yuekeUserUser.getUserPhone());
        if(user_phone==null){
            return  ResultDto.success().setCode(500).setMsg("该用户不存在");
        }
        boolean remove = yuekeUserUserService.remove(yuekeUserUserQueryWrapper);
        return ResultDto.success().setCode(200).setMsg("用户注销成功").setData(remove);
    }
    /**
     * 关注营地 类型3
     * @return
     */
    @PostMapping("/attcamp")
    @ApiOperation(value = "关注营地")
    public ResultDto attcamp(@RequestBody YuekeUserAttention yuekeUserAttention){
        yuekeUserAttention
                .setUserId(yuekeUserAttention.getUserId())
                .setAtwillId(yuekeUserAttention.getAtwillId())
                .setAttentionTypora(3);
        boolean save = yuekeUserAttentionService.save(yuekeUserAttention);
        return ResultDto.success().setCode(200).setMsg("关注营地成功").setData(save);
    }

    /**
     * 关注商品 类型1
     * @return
     */
    @PostMapping("/attshop")
    @ApiOperation(value = "关注商品")
    public ResultDto attshop(@RequestBody YuekeUserAttention yuekeUserAttention){
        yuekeUserAttention
                .setUserId(yuekeUserAttention.getUserId())
                .setAtwillId(yuekeUserAttention.getAtwillId())
                .setAttentionTypora(1);
        boolean save = yuekeUserAttentionService.save(yuekeUserAttention);
        return ResultDto.success().setCode(200).setMsg("关注商品成功").setData(save);
    }

    /**
     * 关注活动 类型2
     * @return
     */
    @PostMapping("/attactivty")
    @ApiOperation(value = "关注活动")
    public ResultDto attactivty(@RequestBody YuekeUserAttention yuekeUserAttention){
        yuekeUserAttention
                .setUserId(yuekeUserAttention.getUserId())
                .setAtwillId(yuekeUserAttention.getAtwillId())
                .setAttentionTypora(2);
        boolean save = yuekeUserAttentionService.save(yuekeUserAttention);
        return ResultDto.success().setCode(200).setMsg("关注活动成功").setData(save);
    }

    /**
     * 关注房车 类型0
     * @return
     */
    @PostMapping("/attroomcar")
    @ApiOperation(value = "关注房车")
    public ResultDto attroomcar(@RequestBody YuekeUserAttention yuekeUserAttention){
        yuekeUserAttention
                .setUserId(yuekeUserAttention.getUserId())
                .setAtwillId(yuekeUserAttention.getAtwillId())
                .setAttentionTypora(0);
        boolean save = yuekeUserAttentionService.save(yuekeUserAttention);
        return ResultDto.success().setCode(200).setMsg("关注房车成功").setData(save);
    }

    /**
     * 我的关注(传入用户id查询我的关注,进行分组显示)
     * @return
     */
    @PostMapping("/mestar")
    @ApiOperation(value = "显示我的关注")
    public ResultDto mestar(@RequestBody YuekeUserAttention yuekeUserAttention){
        QueryWrapper<YuekeUserAttention> yuekeUserAttentionQueryWrapper = new QueryWrapper<>();
        QueryWrapper<YuekeUserAttention> user_id = yuekeUserAttentionQueryWrapper.eq("user_id", yuekeUserAttention.getUserId());
        YuekeUserAttention one = yuekeUserAttentionService.getOne(user_id);
        return ResultDto.success().setCode(200).setMsg("我的关注查看成功").setData(one);
    }
    /**
     * 交易评论(添加评论) 0.表示未评论,1表示已评论
     * @return
     */
    @PostMapping("/amound")
    @ApiOperation(value = "对交易进行评论")
    public ResultDto amound(@RequestBody YuekeUserComment yuekeUserComment){
        yuekeUserComment
                .setUserId(yuekeUserComment.getUserId())
                .setCommentContent(yuekeUserComment.getCommentContent())
                .setUserName(yuekeUserComment.getUserName())
                .setCommentGrade(yuekeUserComment.getCommentGrade())
                .setCommentStatge(1);
        boolean save = yuekeUserCommentService.save(yuekeUserComment);
        return ResultDto.success().setCode(200).setMsg("订单评论成功").setData(save);
    }
    /**
     * 我的评论
     * @return
     */
    @PostMapping("/memonubt")
    @ApiOperation(value = "显示我的评论")
    public ResultDto memonubt(@RequestBody YuekeUserComment yuekeUserComment){
        QueryWrapper<YuekeUserComment> yuekeUserCommentQueryWrapper = new QueryWrapper<>();
        QueryWrapper<YuekeUserComment> comment_id = yuekeUserCommentQueryWrapper.eq("comment_id",yuekeUserComment.getCommentId());
        YuekeUserComment one = yuekeUserCommentService.getOne(comment_id);
        return ResultDto.success().setCode(200).setMsg("我的评论查看成功").setData(one);
    }

    /**
     * 根据id查询用户的优惠卷
     * @param yuekeUserReel
     * @return
     */
    @GetMapping("/queryreel")
    @ApiOperation(value = "显示我的优惠卷")
    public ResultDto mereel(@RequestBody YuekeUserReel yuekeUserReel){
        QueryWrapper<YuekeUserReel> yuekeUserReelQueryWrapper = new QueryWrapper<>();
        QueryWrapper<YuekeUserReel> user_id = yuekeUserReelQueryWrapper.eq("user_id", yuekeUserReel.getUserId());
        YuekeUserReel one = yuekeUserReelService.getOne(user_id);
        return ResultDto.success().setCode(200).setMsg("我的优惠卷查看成功").setData(one);
    }




    /**
     * 找回密码
     * @return
     */
    @PostMapping("/mepassword")
    @ApiOperation(value = "根据手机号找回密码")
    public ResultDto mepassword(@RequestBody YuekeUserUser yuekeUserUser){
        QueryWrapper<YuekeUserUser> yuekeUserUserQueryWrapper = new QueryWrapper<>();
        QueryWrapper<YuekeUserUser> user_phone = yuekeUserUserQueryWrapper.eq("user_phone", yuekeUserUser.getUserPhone());
        //账户是否存在
        if(user_phone == null){
            return ResultDto.success().setCode(500).setMsg("电话不存在").setData(null);
        }
        //校验码是否正确(对校验码进行判断,如果校验码输入正确)
        if(true){
            //找回密码
            YuekeUserUser one = yuekeUserUserService.getOne(user_phone);
            String userPassword = one.getUserPassword();
            return ResultDto.success().setCode(200).setMsg("密码找回成功").setData(userPassword);
        }
        return ResultDto.success().setCode(200).setMsg("密码找回失败").setData("校验码输入错误");
    }

    /**
     * 实名认证(图片上传)
     * @return
     */
    @PostMapping("/realname")
    @ApiOperation(value = "实名认证,上传身份证")
    public ResultDto realname(@RequestBody YuekeUserUser yuekeUserUser){
        QueryWrapper<YuekeUserUser> querywarp = new QueryWrapper<>();
        QueryWrapper<YuekeUserUser> eq = querywarp.eq("user_idcard", yuekeUserUser.getUserIdcard()).eq("user_idcardurl", yuekeUserUser.getUserIdcardurl());
        //判断数据库中的身份id是否为空


        return ResultDto.success().setCode(200).setMsg("实名认证成功");
    }

    /**
     * 申请售后(退款)
     * @return
     */
    @PostMapping("/user/refund")
    @ApiOperation(value = "申请售后退款")
    public ResultDto refund(){
        return ResultDto.success().setCode(200).setMsg("申请售后成功");
    }
    /**
     * 联系客服(websocket)
     * @return
     */
    @PostMapping("/user/mecontact")
    @ApiOperation(value = "联系客服")
    public ResultDto mecontact(){
        return ResultDto.success().setCode(200).setMsg("客服联系成功");
    }

    /**
     * 购物车下单返回数据
     * @return
     */
    @ApiOperation(value = "购物车下单返回数据")
    public ResultDto dealcar(){

        return ResultDto.success().setCode(200).setMsg("交易成功");
    }

    /**
     * 短信校验码
     * 传入联系人手机号,发送短信随机码
     * @return
     */
    @PostMapping("/user/msgcode")
    @ApiOperation(value = "发送短信随机码")
    public ResultDto msgcode(@RequestBody YuekeUserUser yuekeUserUser){

        return ResultDto.success().setCode(200).setMsg("发送成功");
    }
}
