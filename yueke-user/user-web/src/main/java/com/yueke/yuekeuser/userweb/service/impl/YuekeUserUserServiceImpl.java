package com.yueke.yuekeuser.userweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.user.YuekeUserUser;
import com.yueke.yuekeuser.userweb.mapper.YuekeUserUserMapper;
import com.yueke.yuekeuser.userweb.service.YuekeUserUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Service
public class YuekeUserUserServiceImpl extends ServiceImpl<YuekeUserUserMapper, YuekeUserUser> implements YuekeUserUserService {

}
