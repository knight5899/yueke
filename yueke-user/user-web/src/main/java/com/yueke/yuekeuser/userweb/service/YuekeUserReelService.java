package com.yueke.yuekeuser.userweb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.user.YuekeUserReel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
public interface YuekeUserReelService extends IService<YuekeUserReel> {

}
