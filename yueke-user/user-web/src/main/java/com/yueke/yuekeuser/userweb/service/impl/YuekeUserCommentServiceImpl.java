package com.yueke.yuekeuser.userweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.user.YuekeUserComment;
import com.yueke.yuekeuser.userweb.mapper.YuekeUserCommentMapper;
import com.yueke.yuekeuser.userweb.service.YuekeUserCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Service
public class YuekeUserCommentServiceImpl extends ServiceImpl<YuekeUserCommentMapper, YuekeUserComment> implements YuekeUserCommentService {

}
