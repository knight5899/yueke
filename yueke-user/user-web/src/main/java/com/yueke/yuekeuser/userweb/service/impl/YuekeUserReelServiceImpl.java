package com.yueke.yuekeuser.userweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.user.YuekeUserReel;
import com.yueke.yuekeuser.userweb.mapper.YuekeUserReelMapper;
import com.yueke.yuekeuser.userweb.service.YuekeUserReelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Service
public class YuekeUserReelServiceImpl extends ServiceImpl<YuekeUserReelMapper, YuekeUserReel> implements YuekeUserReelService {

}
