package com.yueke.yuekeuser.userweb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.yueke.yuekeuser.userweb.mapper")
@EnableFeignClients
@ComponentScan("com.yueke.yuekecommon.yuekeweb.config")
@ComponentScan("com.yueke.yuekeuser.userweb.controller")
public class UserWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserWebApplication.class, args);
	}

}
