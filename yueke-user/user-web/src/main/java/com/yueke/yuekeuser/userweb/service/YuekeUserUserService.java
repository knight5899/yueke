package com.yueke.yuekeuser.userweb.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.user.YuekeUserUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
public interface YuekeUserUserService extends IService<YuekeUserUser> {

}
