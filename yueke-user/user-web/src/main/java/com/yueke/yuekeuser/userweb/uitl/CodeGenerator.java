package com.yueke.yuekeuser.userweb.uitl;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;



/**
 * @program: yueke-parent
 * @description: 代码生成器
 * @author: XiYang
 * @create: 2020-10-12 19:17
 **/

public class CodeGenerator {
    public static void main(String[] args) {
        //1. 构建代码自动生成器对象
        AutoGenerator mpg = new AutoGenerator();

        //3. 配置策略
        //3.1 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");//项目路径
        gc.setOutputDir(projectPath+"/src/main/java");//输出目录
        gc.setAuthor("xiyang");
        gc.setOpen(false); //不打文件夹
        gc.setFileOverride(false); //是否覆盖
        gc.setServiceName("%sService"); //去掉Service的I前缀
        gc.setIdType(IdType.AUTO);//主键策略
        gc.setDateType(DateType.ONLY_DATE);
        //gc.setSwagger2(true);//开启swager
        mpg.setGlobalConfig(gc);

        //3.2 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://81.68.251.241:3306/yueke?useUnicode=true&characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8");
        dsc.setUsername("root");
        dsc.setPassword("root123");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        //3.3 包的配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName("blog");//设置模块名
        pc.setParent("com.yueke.common.core.demo");//放在哪个包下
        pc.setEntity("domain"); //domain包
        pc.setMapper("mapper"); //mapper包
        pc.setService("service"); //service包
        pc.setController("controller"); //controller包
        mpg.setPackageInfo(pc);


        //3.4 策略配置
        // 策略配置

        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude(
//                "yueke_activity_activity",
//                "yueke_activity_orders",
//                "yueke_camp_camp",
//                "yueke_camp_manager",
//                "yueke_platfor_activmapper
//serviceityincome",
//                "yueke_platfor_commentmanagement",
//                "yueke_platfor_marketrevenue",
//                "yueke_platfor_notice",
//                "yueke_platfor_promptuser",
//                "yueke_platfor_rvmaintenance",
//                "yueke_platfor_rvrevenue",
                //"yueke_rv",
//                "yueke_rv_deposit_record",
//                "yueke_rv_orders",
//                "yueke_rv_orders_item"
//                "yueke_shop_classify",
//                "yueke_shop_coupon",
//                "yueke_shop_coupon_rule",
//                "yueke_shop_goods",
//                "yueke_shop_measure_unit",
//                "yueke_shop_order_item",
//                "yueke_shop_orders",
//                "yueke_user_attention",
//                "yueke_user_comment",
//                "yueke_user_reel",
                "yueke_user_user"
        );//设置需要哪些表映射(重点)
        strategy.setNaming(NamingStrategy.underline_to_camel); //设置包的命名规则,下滑线转驼峰命名
        strategy.setColumnNaming(NamingStrategy.underline_to_camel); //数据列的名字,下滑钱转驼峰命名
        //strategy.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        strategy.setEntityLombokModel(true); //是否支持Lombok链式编程
        //strategy.setLogicDeleteFieldName("deleted");//自动配置逻辑删除
        //TableFill gmtCreate = new TableFill("gmt_create", FieldFill.INSERT);//自动填充配置
        //TableFill gmtModified = new TableFill("gmt_modified", FieldFill.INSERT_UPDATE);//自动填充配置
        //ArrayList<TableFill> arraylist = new ArrayList<>();
        //arraylist.add(gmtCreate);
        //arraylist.add(gmtModified);
        //strategy.setTableFillList(arraylist); //配置表的创建时间策略
        //strategy.setVersionFieldName("version");//配置乐观锁
        strategy.setRestControllerStyle(true);//开启RestFul的驼峰命名
        //strategy.setControllerMappingHyphenStyle(true);//localhost:8080/hello_id_2
        mpg.setStrategy(strategy);

        //2. 执行代码生成器
        mpg.execute();


    }
}


//需要的依赖
/*
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.2.0</version>
</dependency>
<dependency>
    <groupId>org.apache.velocity</groupId>
    <artifactId>velocity-engine-core</artifactId>
    <version>2.1</version>
</dependency>
<!--Mybatis Plus的起步依赖-->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.3.2</version>
</dependency>

 */