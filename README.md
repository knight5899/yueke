# yueke

yueke房车是一款xxxxx

> 项目名: 悦客房车 
>
> 项目介绍: “悦客房车”项目是一个以营地为消费中心,房车为住宿条件的韭菜收割项目.可 以向用户推送各种热门营地以及商品特产和房车住宿.管理员可以在后台进行日常管理,活动推送等刺激用户消费的消息,支持商品在线下单,在线退款,活动报 名,房车提前预定等功能. 支持线上即时聊天通信,支持定位导航. 
>
> 技术选型: 本项目前后端分离,后端采用SpringCloud Alibaba微服务进行架构,Redis作为 缓存,RocketMQ作为消息中间件,百度地图API定位,SpringBoot-WebSocket实 时通信,MySQL作为数据库,MyBatisPlus中间件操作数据库,前端显示界面采用 了Vue2.5,以及ElementUI框架,基于Element-admin的二次开发作为后台管理 展示 
>
> 工作内容: 
>
> 1. 参与需求分析,参与数据库的设计 
> 2.  负责公共服务的编写 
> 3.  负责用户微服务的编写

