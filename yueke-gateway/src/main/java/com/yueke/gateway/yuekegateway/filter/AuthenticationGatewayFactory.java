package com.yueke.gateway.yuekegateway.filter;

import com.yueke.common.core.exception.WoniuAuthencticationException;
import com.yueke.common.core.exception.WoniuExceptionCode;
import com.yueke.common.core.util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @program: yueke-parent
 * @description:
 * @author: XiYang
 * @create: 2020-10-14 11:02
 **/
@Component
public class AuthenticationGatewayFactory extends AbstractGatewayFilterFactory<AuthenticationGatewayFactory.Config> {

    public AuthenticationGatewayFactory() {
        super(Config.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("name");
    }

    /**
     *  认证拦截器，判断是否登录
     *
     */
    @Override
    public GatewayFilter apply(Config config) {
        return (exchange,chain)->{
            ServerHttpRequest serverHttpRequest = exchange.getRequest();
            String path = serverHttpRequest.getPath().toString();
            System.out.println("path = " + path);
            //登录请求，放行
            if (StringUtils.contains(path,"/user/login")||StringUtils.contains(path,"/user/demo")||
                    StringUtils.contains(path,"/gym/unfilter")||StringUtils.contains(path,"/backstage/unfilter")){
                return chain.filter(exchange);
            }
            //其他请求，进行权限验证
            String token = serverHttpRequest.getHeaders().getFirst("X-Token");
            if (StringUtils.isBlank(token)) {
                throw new WoniuAuthencticationException(WoniuExceptionCode.USER_AUTHE_NOT_LOGIN);
            }
            try {
                JwtUtil.parseToken(token);
            } catch (SignatureException e) {
                throw new WoniuAuthencticationException(WoniuExceptionCode.USER_AUTHE_TOKEN_PARSE);
            }catch (ExpiredJwtException e) {
                throw new WoniuAuthencticationException(WoniuExceptionCode.USER_AUTHE_TOKEN_EXPIRE);
            }catch (Exception e) {
                throw new WoniuAuthencticationException(WoniuExceptionCode.SYS_ERROR);
            }
            return chain.filter(exchange);
        };

    }


    public static class Config {
        private String name;
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
}
