package com.yueke.gateway.yuekegateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YuekeGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(YuekeGatewayApplication.class, args);
    }

}
