package com.yueke.gateway.yuekegateway.config;

import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import java.util.Arrays;
import java.util.List;

/**
 * @program: yueke-parent
 * @description: 配置 Swagger下拉列表的资源信息
 * @author: XiYang
 * @create: 2020-10-13 18:50
 **/
@Component
public class ResourceProvider implements SwaggerResourcesProvider {
    @Override
    public List<SwaggerResource> get() {
        return Arrays.asList(
            createSwaggerResource("用户-黎秦赭","/user/v2/api-docs","2.0"),
            createSwaggerResource("营地-叶青","/camp/v2/api-docs","2.0"),
            createSwaggerResource("集市-文豪","/shop/v2/api-docs","2.0"),
            createSwaggerResource("平台-胡皓月","/platfor/v2/api-docs","2.0")
        );
    }

    private SwaggerResource createSwaggerResource(String name,String url,String version){
        SwaggerResource resource = new SwaggerResource();
        resource.setName(name);
        resource.setUrl(url);
        resource.setSwaggerVersion(version);
        return resource;
    }
}
