package com.yueke.commom.ribbon.config;

import com.netflix.loadbalancer.IRule;
import com.yueke.commom.ribbon.rules.ClusterWeightRule;
import org.springframework.context.annotation.Bean;

/**
 * @program: yueke-parent
 * @description: RibbonConfig 全局负载均衡器配置
 * @author: XiYang
 * @create: 2020-10-13 19:13
 **/
public class RibbonConfig {
    /**
     * @return 自定义负载均衡策略
     */
    @Bean
    public IRule getRule(){
        return new ClusterWeightRule();
    }
}
