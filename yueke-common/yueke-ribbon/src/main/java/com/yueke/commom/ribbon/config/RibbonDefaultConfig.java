package com.yueke.commom.ribbon.config;

import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Configuration;

/**
 * @program: yueke-parent
 * @description: 全局负载均衡配置
 * @author: XiYang
 * @create: 2020-10-13 19:14
 **/
@Configuration
@RibbonClients(defaultConfiguration = RibbonConfig.class)
public class RibbonDefaultConfig {

}