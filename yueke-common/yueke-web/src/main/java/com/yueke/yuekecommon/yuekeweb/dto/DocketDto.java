package com.yueke.yuekecommon.yuekeweb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 胡八一
 * @date 2020/10/16 16:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "swagger")
public class DocketDto {
    private String title; //标题
    private String version = "1.0"; //版本
    private String desc; //描述
    private String basePath; //包路径
    //private Author author;
    private String name;
    private String url;
    private String email;
}
