package com.yueke.yuekecommon.yuekeweb.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @program: yueke-parent
 * @description:
 * @author: XiYang
 * @create: 2020-10-17 10:27
 **/
public class TokenUtil {
    public static String getToken(){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return attributes.getRequest().getHeader("X-Token");
    }
}
