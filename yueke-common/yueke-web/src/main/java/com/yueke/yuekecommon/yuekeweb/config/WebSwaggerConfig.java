package com.yueke.yuekecommon.yuekeweb.config;


import com.yueke.yuekecommon.yuekeweb.dto.DocketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.List;

/**
 * @author 胡八一
 * @date 2020/10/16 16:27
 */
@Configuration
@EnableSwagger2
@EnableConfigurationProperties(DocketDto.class)
public class WebSwaggerConfig {

    @Autowired
    private DocketDto docketDto;

    @Bean
    public Docket createDocket(){
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        List<Parameter> ps = Arrays.asList(
                new ParameterBuilder().parameterType("header").name("X-Token").build()
        );

        docket.apiInfo(new ApiInfoBuilder()
            .title(docketDto.getTitle())
            .description(docketDto.getDesc())
            .contact(new Contact(docketDto.getName(),docketDto.getUrl(),docketDto.getEmail()))
            .version(docketDto.getVersion())
            .build()
        )
        //.globalOperationParameters(ps)
        .select()
            .apis(RequestHandlerSelectors.basePackage(docketDto.getBasePath()))
            .paths(PathSelectors.any())
            .build();
        return docket;
    }


}
