package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_orders_or_order_item")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopOrdersOrOrdersItem {

    @TableId(value = "orders_or_order_item_id", type = IdType.AUTO)
    private Integer ordersOrOrderItemId;

    private Integer ordersId;

    private Integer orderItemId;
}
