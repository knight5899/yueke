package com.yueke.common.core.exception;

/**
 * WoniuException 自定义异常状态码
 */
public enum WoniuExceptionCode {
    /**
     * 自定义异常
     */

    USER_AUTHE_ERROR(2001, "用户名或密码错误"),
    USER_AUTHE_TOKEN_PARSE(2002, "token解析错误"),
    USER_AUTHE_TOKEN_EXPIRE(2003, "token过期"),
    USER_AUTHE_NOT_LOGIN(2004, "未登录"),

    SENTINEL_BLOCK(3001,"流控异常"),

    JSON_PARSE(4001,"json解析异常"),

    SYS_URL_NOT_FOUNDED(404,"访问资源不存在"),
    SYS_ERROR(500, "出现异常");


    private Integer code;
    private String msg;

    WoniuExceptionCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
