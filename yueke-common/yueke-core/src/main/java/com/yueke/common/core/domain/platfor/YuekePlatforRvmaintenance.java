package com.yueke.common.core.domain.platfor;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YuekePlatforRvmaintenance implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "rvmaintenance_id", type = IdType.AUTO)
    private Integer rvmaintenanceId;

    @TableField(value = "rvmaintenance_rvId",updateStrategy = FieldStrategy.IGNORED)
    private Integer rvmaintenanceRvid;

    @TableField(value = "rvmaintenance_rvFault",updateStrategy = FieldStrategy.IGNORED)
    private String rvmaintenanceRvfault;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date rvmaintenanceTime;

    @TableField(value = "rvmaintenance_completionTime",updateStrategy = FieldStrategy.IGNORED)
    private Date rvmaintenanceCompletiontime;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String rvmaintenanceProgress;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String rvmaintenanceState;


}
