package com.yueke.common.core.domain.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeUserAttention implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "attention_id", type = IdType.AUTO)
    private Integer attentionId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 存房车,商品,活动,营地的id值
     */
    private Integer atwillId;
    /**
     * 关注类型0.房车,1商品2活动3营地
     */
    private Integer attentionTypora;


}
