package com.yueke.common.core.domain.platfor;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("yueke_platfor_commentmanagement")
public class YuekePlatforCommentmanagement implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "commentmanagement_id", type = IdType.AUTO)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer commentmanagementId;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date commentmanagementTime;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String commentmanagementContent;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String commentmanagementState;

    @TableField(value = "commentmanagement_userId",updateStrategy = FieldStrategy.IGNORED)
    private Integer commentmanagementUserid;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String commentmanagementInformation;


}
