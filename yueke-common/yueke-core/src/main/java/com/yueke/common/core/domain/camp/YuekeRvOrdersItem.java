package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeRvOrdersItem implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 订单项id
     */
    @TableId(value = "yueke_rv_orders_item_id", type = IdType.AUTO)
    private Integer yuekeRvOrdersItemId;

    /**
     * 订单id
     */
    private Integer yuekeRvOrdersId;

    /**
     * 入住用户信息
     */
    private String yuekeRvOrdersItemUserInfo;

    /**
     * 房车类型
     */
    private String yuekeRvOrdersRvType;

    /**
     * 房车编号
     */
    private Integer yuekeRvOrdersRvId;

    /**
     * 房车最大入住人数
     */
    private Integer yuekeRvOrdersRvOccupancyRange;

    /**
     * 所属城市
     */
    private String yuekeRvOrdersCity;

    /**
     * 所属营地
     */
    private Integer yuekeRvOrdersCampId;

    /**
     * 单价
     */
    private BigDecimal yuekeRvOrdersPrice;

    /**
     * 入住时间
     */
    private Date yuekeRvOrdersIntime;

    /**
     * 退房时间
     */
    private Date yuekeRvOrdersOuttime;

    /**
     * 入住天数
     */
    private Integer yuekeRvOrdersDaynum;

    /**
     * 入住人数
     */
    private Integer yuekeRvOrdersCustomerNum;


}
