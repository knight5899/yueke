package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_coupon")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopCoupon implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 优惠券id
     */
    @TableId(value = "coupon_id", type = IdType.AUTO)
    private Integer couponId;

    /**
     * 优惠规则id
     */
    private Integer couponRuleId;

    /**
     * 优惠券名称
     */
    private Integer couponName;

    /**
     * 优惠券开始时间
     */
    private Long couponStartTime;

    /**
     * 优惠券结束时间
     */
    private Long couponEndTime;

    /**
     * 数量
     */
    private Integer couponCount;


}
