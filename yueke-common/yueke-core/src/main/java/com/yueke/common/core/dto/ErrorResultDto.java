package com.yueke.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: yueke-parent
 * @description: 异常响应结果
 * @author: XiYang
 * @create: 2020-10-13 15:18
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorResultDto {
    /**
     * 异常码
     */
    private Integer errorCode;

    /**
     * 异常信息
     */
    private String errorMessage;
}
