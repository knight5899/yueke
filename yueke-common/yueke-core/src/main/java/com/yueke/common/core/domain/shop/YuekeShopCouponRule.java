package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_coupon_rule")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopCouponRule implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 优惠规则id
     */
    @TableId(value = "coupon_rule_id", type = IdType.AUTO)
    private Integer couponRuleId;

    /**
     * 使用门槛
     */
    private BigDecimal couponRuleThreshold;

    /**
     * 使用方式
     */
    private String couponRuleManner;


}
