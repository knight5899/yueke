package com.yueke.common.core.domain.platfor;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YuekePlatforPromptuser implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "promptuser_id", type = IdType.AUTO)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer promptuserId;

    @TableField(value = "promptuser_userId",updateStrategy = FieldStrategy.IGNORED)
    private Integer promptuserUserid;

    @TableField(value = "promptuser_userName",updateStrategy = FieldStrategy.IGNORED)
    private String promptuserUsername;

    @TableField(value = "promptuser_orderId",updateStrategy = FieldStrategy.IGNORED)
    private Integer promptuserOrderid;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String promptuserType;

    @TableField(value = "promptuser_outTime",updateStrategy = FieldStrategy.IGNORED)
    private Date promptuserOuttime;


}
