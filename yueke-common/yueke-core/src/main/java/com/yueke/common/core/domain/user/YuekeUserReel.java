package com.yueke.common.core.domain.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeUserReel implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 用户优惠卷id
     */
    @TableId(value = "reel_id", type = IdType.AUTO)
    private Integer reelId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 优惠卷类型(0.活动优惠卷 1.商品优惠卷 2.房车优惠卷)
     */
    private Integer reelType;

    /**
     * 优惠卷使用说明
     */
    private String reelContent;

    /**
     * 优惠卷数量
     */
    private Integer reelNumber;

    /**
     * 领取时间
     */
    private Date reelCreat;

    /**
     * 有效时间
     */
    private String reelUse;

}
