package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeCampManager implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 管理员id
     */
    @TableId(value = "yueke_camp_manager_id", type = IdType.AUTO)
    private Integer yuekeCampManagerId;

    /**
     * 管理员所在营地id
     */
    private Integer yuekeCampId;

    /**
     * 管理员姓名
     */
    private String yuekeCampManagerName;

    /**
     * 管理员电话
     */
    private String yuekeCampManagerPhone;

    /**
     * 管理员密码
     */
    private String yuekeCampManagerPassword;

    /**
     * 管理员状态
     */
    private String yuekeCampManagerStatus;

    /**
     * 账号生成时间
     */
    private Date yuekeCampManagerCreatetime;

    /**
     * 账号注销时间
     */
    private Date yuekeCampManagerLogoutime;


}
