package com.yueke.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: yueke-parent
 * @description: 自定义结果工具类
 * @author: XiYang
 * @create: 2020-10-10 14:28
 **/

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResultDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 返回状态码,消息,对象
     */
    private Integer code;
    private String msg;
    private Object data;

    //
    public static ResultDto success(){
        return new ResultDto();
    }

    public ResultDto setCode(Integer code) {
        this.code = code;
        return this;
    }

    public ResultDto setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public ResultDto setData(Object obj) {
        this.data = obj;
        return this;
    }

}
