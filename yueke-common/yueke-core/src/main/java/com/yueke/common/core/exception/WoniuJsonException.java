package com.yueke.common.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: yueke-parent
 * @description:
 * @author: XiYang
 * @create: 2020-10-13 19:03
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class WoniuJsonException extends WoniuException{
    private static final long serialVersionUID = 1L;
    private Integer code;

    public WoniuJsonException(Integer code, String message) {
        super(code, message);
        this.code = code;
    }

    public WoniuJsonException(WoniuExceptionCode code) {
        super(code);
        this.code = code.getCode();
    }
}
