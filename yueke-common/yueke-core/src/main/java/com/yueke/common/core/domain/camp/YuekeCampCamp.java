package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeCampCamp implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 营地编号
     */
    @TableId(value = "yueke_camp_id", type = IdType.AUTO)
    private Integer yuekeCampId;

    /**
     * 营地名称
     */
    private String yuekeCampName;

    /**
     * 营地简介
     */
    private String yuekeCampIntroduction;

    /**
     * 营地详细介绍
     */
    private String yuekeCampDetailIntroduction;

    /**
     * 营地负责人电话
     */
    private String yuekeCampPrincipalPhone;

    /**
     * 营地联系人姓名
     */
    private String yuekeCampPrincipalName;

    /**
     * 营地所在城市
     */
    private String yuekeCampCity;

    /**
     * 营地详细地址
     */
    private String yuekeCampDetailAddr;

    /**
     * 营地面积
     */
    private Double yuekeCampArea;

    /**
     * 营地状态
     */
    private Integer yuekeCampUpDownStatus;

    /**
     * 营地上线时间
     */
    private Date yuekeCampUpTime;

    /**
     * 营地下线时间
     */
    private Date yuekeCampDownlineTime;

    /**
     * 营地照片
     */
    @TableField("yueke_camp_imgAddrs")
    private String yuekeCampImgaddrs;


}
