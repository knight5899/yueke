package com.yueke.common.core.domain.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeUserComment implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 评论id
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Integer commentId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 评论内容
     */
    private String commentContent;

    /**
     * 冗余字段用户账号名
     */
    private String userName;

    /**
     * x星评论(1,2,3,4,5)
     */
    private Integer commentGrade;

    /**
     * 消费类型(0.活动订单 1房车订单 2商品订单)
     */
    private Integer commentType;

    /**
     * 评论状态0.未评论 1已评论
     */
    private Integer commentStatge;


}
