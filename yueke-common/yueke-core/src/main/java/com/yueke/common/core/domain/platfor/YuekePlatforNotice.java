package com.yueke.common.core.domain.platfor;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YuekePlatforNotice implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "notice_id", type = IdType.AUTO)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer noticeId;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String noticeTheme;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String noticeDepartment;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String noticeContent;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date noticeTime;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String noticeState;


}
