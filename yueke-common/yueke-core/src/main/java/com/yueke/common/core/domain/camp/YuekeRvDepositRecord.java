package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeRvDepositRecord implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 押金记录编号
     */
    @TableId(value = "yueke_rv_deposit_record_id", type = IdType.AUTO)
    private Integer yuekeRvDepositRecordId;

    /**
     * 订单id
     */
    private Integer yuekeRvDepositRecordOrdersId;

    /**
     * 押金金额
     */
    private String yuekeRvDepositRecordMoney;

    /**
     * 缴纳时间
     */
    private Date yuekeRvDepositRecordTime;

    /**
     * 押金状态
     */
    private Integer yuekeRvDepositStatus;

    /**
     * 退还时间
     */
    private Date yuekeRvDepositRecordBackTime;


}
