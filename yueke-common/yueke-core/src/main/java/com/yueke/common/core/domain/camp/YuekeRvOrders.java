package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeRvOrders implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 订单id
     */
    @TableId(value = "yueke_rv_orders_id", type = IdType.AUTO)
    private Integer yuekeRvOrdersId;

    /**
     * 用户id
     */
    private Integer yuekeRvOrdersUserId;

    /**
     * 营地编号
     */
    private Integer yuekeCampId;

    /**
     * 入住总人数
     */
    private Integer yuekeRvOrdersUserNum;

    /**
     * 用户联系方式
     */
    private String yuekeRvOrdersPhone;

    /**
     * 到店时间
     */
    private Date yuekeRvOrdersArriveTime;

    /**
     * 所在城市
     */
    private String yuekeRvOrdersCity;

    /**
     * 总价
     */
    private BigDecimal yuekeRvOrdersTotalprice;

    /**
     * 订单生成时间
     */
    private Date yuekeRvOrdersCreatetime;

    /**
     * 订单状态
     */
    private Integer yuekeRvOrdersStatus;

    /**
     * 订单支付状态
     */
    private Integer yuekeRvOrdersPaystatus;

    /**
     * 支付方式
     */
    private String yuekeRvOrdersPaymethod;

    /**
     * 押金缴纳时间
     */
    private Date yuekeRvDepositRecordTime;

    /**
     * 押金退还时间
     */
    private Date yuekeRvDepositRecordBackTime;

    /**
     * 押金金额
     */
    private BigDecimal yuekeRvDepositRecordMoney;

    /**
     * 押金状态
     */
    private Integer yuekeRvOrdersDepositStatus;

    /**
     * 联系人身份证号码
     */
    private String yuekeRvOrdersUserIdentity;

    /**
     * 入住时间
     */
    private Date yuekeRvOrdersIntime;

    /**
     * 退房时间
     */
    private Date yuekeRvOrdersOuttime;

    /**
     * 入住天数
     */
    private Integer yuekeRvOrdersDaynum;
    /**
     * 房车订单uuid
     */
    private String yuekeRvOrdersUuid;

    /**
     * 房车编号
     */
    private Integer yuekeRvOrdersRvId;
}
