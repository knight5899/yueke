package com.yueke.common.core.domain.platfor;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YuekePlatforCommentnotes implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "commentnotes_id", type = IdType.AUTO)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer commentnotesId;

    @TableField(value = "commentnotes_userId",updateStrategy = FieldStrategy.IGNORED)
    private Integer commentnotesUserid;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date commentnotesTime;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String commentnotesInformation;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String commentnotesState;


}
