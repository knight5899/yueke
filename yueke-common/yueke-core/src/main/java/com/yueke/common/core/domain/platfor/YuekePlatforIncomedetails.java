package com.yueke.common.core.domain.platfor;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YuekePlatforIncomedetails implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "incomedetails_id", type = IdType.AUTO)
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer incomedetailsId;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date incomedetailsTime;

    @TableField(value = "incomedetails_orderId",updateStrategy = FieldStrategy.IGNORED)
    private Integer incomedetailsOrderid;

    @TableField(value = "incomedetails_userId",updateStrategy = FieldStrategy.IGNORED)
    private Integer incomedetailsUserid;

    @TableField(value = "incomedetails_userName",updateStrategy = FieldStrategy.IGNORED)
    private String incomedetailsUsername;

    @TableField(value = "incomedetails_orderConsumption",updateStrategy = FieldStrategy.IGNORED)
    private Double incomedetailsOrderconsumption;

    @TableField(value = "incomedetails_orderCost",updateStrategy = FieldStrategy.IGNORED)
    private Double incomedetailsOrdercost;

    @TableField(value = "incomedetails_orderState",updateStrategy = FieldStrategy.IGNORED)
    private String incomedetailsOrderstate;

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String incomedetailsType;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double incomedetailsProfit;


}
