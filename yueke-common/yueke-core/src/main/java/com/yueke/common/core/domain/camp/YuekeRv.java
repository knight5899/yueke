package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeRv implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 房车id
     */
    @TableId(value = "yueke_rv_id", type = IdType.AUTO)
    private Integer yuekeRvId;

    /**
     * 投放营地id
     */
    private Integer yuekeCampId;

    /**
     * 房车所在营地名称
     */
    private String yuekeCampName;

    /**
     * 房车类型
     */
    private String yuekeRvType;

    /**
     * 房车最大入住人数
     */
    private Integer yuekeRvOccupancyMaxnum;

    /**
     * 房车所在城市
     */
    private String yuekeRvCity;

    /**
     * 房车入住每日单价
     */
    private BigDecimal yuekeRvPrice;

    /**
     * 房车状态
     */
    private Integer yuekeRvStatus;

    /**
     * 房车下线时间
     */
    private Date yuekeRvDowntime;

    /**
     * 房车上线时间
     */
    private Date yuekeRvCreatetime;

    /**
     * 房车详细介绍
     */
    private String yuekeRvIntroduction;

    /**
     * 房车押金金额
     */
    private BigDecimal yuekeRvDepositMon;

    /**
     * 房车图片地址
     */
    private String yuekeRvImgaddrs;


}
