package com.yueke.common.core.exception;

import lombok.Builder;
import lombok.Data;

/**
 * @program: yueke-parent
 * @description: ResultError 自定义异常结果封装类
 * @author: XiYang
 * @create: 2020-10-13 19:00
 **/
@Data
@Builder
public class ResultError {
    private Integer code;
    private String msg;

    public static ResultError error(WoniuExceptionCode code){
        return ResultError.builder().code(code.getCode()).msg(code.getMsg()).build();
    }


    public static ResultError errorExec(WoniuException e){
        return ResultError.builder().code(e.getCode()).msg(e.getMessage()).build();
    }
}
