package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_goods")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopGoods implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 商品id
     */
    @TableId(value = "goods_id", type = IdType.AUTO)
    private Integer goodsId;

    /**
     * 营地id
     */
    private Integer campsiteId;

    /**
     * 分类id
     */
    private Integer classifyId;

    /**
     * 商品编号
     */
    private String goodsNumber;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品副标题
     */
    private String goodsSubhead;

    /**
     * 商品图片
     */
    private String goodsPicture;

    /**
     * 商品售价
     */
    private BigDecimal goodsPrice;

    /**
     * 商品库存
     */
    private Integer goodsInventory;

    /**
     * 商品计量单位
     */
    private String goodsMeasureUnit;

    /**
     * 商品销量
     */
    private Integer goodsSales;

    /**
     * 商品状态
     */
    private String goodsStatus;


}
