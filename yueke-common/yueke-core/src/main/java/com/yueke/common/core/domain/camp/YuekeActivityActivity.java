package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeActivityActivity implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 活动id
     */
    @TableId(value = "yueke_activity_id", type = IdType.AUTO)
    private Integer yuekeActivityId;

    /**
     * 活动主题
     */
    private String yuekeActivityTittle;



    /**
     * 活动简介
     */
    private String yuekeActivityIntroduction;

    /**
     * 活动详情
     */
    private String yuekeActivityDetail;

    /**
     * 活动门票价格
     */
    private BigDecimal yuekeActivityTicketPrice;

    /**
     * 活动开始时间
     */
    private Date yuekeActivityStartTime;

    /**
     * 活动结束时间
     */
    private Date yuekeActivityEndTime;



    /**
     * 营地举办营地id
     */
    private Integer yuekeActivityCampId;

    /**
     * 活动状态
     */
    private Integer yuekeActivityStatus;

    /**
     * 活动门票总数量
     */
    private Integer yuekeActivityTicketNum;

    /**
     * 活动门票已购买票数
     */
    private String yuekeActivityTicketBoughtNum;

    /**
     * 活动负责人姓名
     */
    private String yuekeActivityManagerName;

    /**
     * 活动负责人联系电话
     */
    private String yuekeActivityManagerPhone;

    /**
     * 活动图片地址
     */
    private String yuejeActivityImgaddrs;


}
