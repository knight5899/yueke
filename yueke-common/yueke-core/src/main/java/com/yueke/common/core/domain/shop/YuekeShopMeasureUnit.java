package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_measure_unit")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopMeasureUnit implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 计量单位id
     */
    @TableId(value = "measure_unit_id", type = IdType.AUTO)
    private Integer measureUnitId;

    /**
     * 计量单位名称
     */
    private String measureUnitName;


}
