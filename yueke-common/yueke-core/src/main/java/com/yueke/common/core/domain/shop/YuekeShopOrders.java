package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_orders")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopOrders implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 订单id
     */
    @TableId(value = "orders_id", type = IdType.AUTO)
    private Integer ordersId;

    /**
     * 订单编号
     */
    private String ordersNum;

    /**
     * 订单总价
     */
    private BigDecimal ordersSumPrice;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 订单提交时间
     */
    private Long ordersStartTime;

    /**
     * 订单付款时间
     */
    private Long ordersPaymentTime;

    /**
     * 订单状态
     */
    private String ordersStatus;


}
