package com.yueke.common.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: yueke-parent
 * @description: 自定义的认证异常处理
 * @author: XiYang
 * @create: 2020-10-13 19:00
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class WoniuAuthencticationException extends WoniuException{
    private static final long serialVersionUID = 1L;
    private Integer code;

    public WoniuAuthencticationException(Integer code, String message) {
        super(code, message);
        this.code = code;
    }

    public WoniuAuthencticationException(WoniuExceptionCode code) {
        super(code);
        this.code = code.getCode();
    }
}
