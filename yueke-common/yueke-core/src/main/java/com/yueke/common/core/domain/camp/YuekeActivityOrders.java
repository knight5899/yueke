package com.yueke.common.core.domain.camp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeActivityOrders implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 活动订单id
     */
    @TableId(value = "yueke_activity_orders_id", type = IdType.AUTO)
    private Integer yuekeActivityOrdersId;

    /**
     * 活动id
     */
    private Integer yuekeActivityId;

    /**
     * 活动所属营地id
     */
    private Integer yuejeActivityCampId;

    /**
     * 用户id
     */
    private Integer yuekeActivityUserId;

    /**
     * 活动下单时间
     */
    private Date yuekeActivityOrdersCreateTime;

    /**
     * 活动门票单价
     */
    private BigDecimal yuekeActivityTicketPrice;

    /**
     * 门票购买数量
     */
    private Integer yuekeActivityTicketNum;

    /**
     * 订单总价
     */
    private BigDecimal yuekeActivityTicketTotalMoney;

    /**
     * 活动门票入场码
     */
    private String yuekeActivityTicketCode;

    /**
     * 订单支付状态
     */
    private Integer yuekeActivityOrdersPaystatus;


}
