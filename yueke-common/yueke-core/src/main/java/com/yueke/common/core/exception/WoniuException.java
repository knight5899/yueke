package com.yueke.common.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @program: yueke-parent
 * @description: WoniuException 自定义异常
 * @author: XiYang
 * @create: 2020-10-13 19:02
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class WoniuException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private Integer code;

    public WoniuException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public WoniuException(WoniuExceptionCode code) {
        super(code.getMsg());
        this.code = code.getCode();
    }
}