package com.yueke.common.core.domain.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YuekeUserUser implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 用户账户名
     */
    private String userName;

    /**
     * 用户头像地址
     */
    private String userImgurl;

    /**
     * 用户身份证号
     */
    private String userIdcard;

    /**
     * 用户身份证图片地址
     */
    private String userIdcardurl;

    /**
     * 用户性别
     */
    private String userSex;

    /**
     * 用户密码
     */
    private String userPassword;

    /**
     * 用户qq,用于三方登录
     */
    private String userQq;

    /**
     * 用户状态,是否认证(0.已认证 1.未认证)身份认证后才可以住宿房车
     */
    private Integer userStatge;

    /**
     * 用户邮箱
     */
    private String userEmail;

    /**
     * 用户在线状态(0.在线 1.离线)用于我的好友分组
     */
    private Integer userWire;

    /**
     * 用户真实姓名
     */
    private String userTureName;

    /**
     * 用户是否在网(0.逻辑在网 1.逻辑注销)
     */
    private String userNet;

    /**
     * 用户电话
     */
    private String userPhone;


}
