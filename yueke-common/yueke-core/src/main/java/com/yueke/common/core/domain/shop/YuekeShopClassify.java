package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 分类id
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_classify")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopClassify implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 分类id
     */
    @TableId(value = "classify_id", type = IdType.AUTO)
    private Integer classifyId;

    /**
     * 分类名称
     */
    private String classifyName;


}
