package com.yueke.common.core.domain.shop;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiyang
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yueke_shop_order_item")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class YuekeShopOrderItem implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 订单项id
     */
    @TableId(value = "order_item_id", type = IdType.AUTO)
    private Integer orderItemId;

    /**
     * 营地id
     */
    private Integer campsiteId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 订单项商品价格
     */
    private BigDecimal orderItemPrice;

    /**
     * 订单项商品数量
     */
    private Integer orderItemCount;

    /**
     * 订单项商品小计
     */
    private BigDecimal orderItemSumPrice;


}
