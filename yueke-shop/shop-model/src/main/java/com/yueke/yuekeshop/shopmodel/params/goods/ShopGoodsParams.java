package com.yueke.yuekeshop.shopmodel.params.goods;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopGoodsParams {

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品分类id
     */
    @NotNull
    private Integer classifyId;

    /**
     * 商品名称
     */
    @NotNull
    private String goodsName;

    /**
     * 商品售价
     */
    @NotNull
    private String goodsSubhead;

    /**
     * 商品价格
     */
    @NotNull
    private BigDecimal goodsPrice;

    /**
     * 商品库存
     */
    @NotNull
    private Integer goodsInventory;

    /**
     * 商品规格
     */
    @NotNull
    private String goodsMeasureUnit;

    /**
     * 营地id
     */
    @NotNull
    private Integer campsiteId;

    /**
     * 商品图片集合
     */
    @NotNull
    private List goodsPictures;




}
