package com.yueke.yuekeshop.shopmodel.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RuleDto {

    private Integer id;

    private String msg;

}
