package com.yueke.yuekeshop.shopmodel.params.orders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrdersListParams {

  private   Integer	ordersPageStart;
  private   Integer	ordersPageSize;
  private   Integer	campsiteId;
  private   String	ordersStatus;

}
