package com.yueke.yuekeshop.shopmodel.params.goods;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoodsIdOrNumParams {

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品编号
     */
    private String goodsNumber;

}
