package com.yueke.yuekeshop.shopmodel.dto.orders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodsOrdersItemDto {

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 生成订单商品价格
     */
    private Double orderItemPrice;

    /**
     * 生成订单购买数量
     */
    private Integer orderItemCount;

    /**
     * 商品小计
     */
    private Double orderItemSumPrice;
}
