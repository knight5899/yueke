package com.yueke.yuekeshop.shopmodel.dto.goods;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShopGoodsDto{

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品编号
     */
    private String goodsNumber;

    /**
     * 商品图片
     */
    private String goodsPicture;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 营地名称
     */
    private Integer campsiteId;

    /**
     * 商品售价
     */
    private BigDecimal goodsPrice;

    /**
     * 分类id
     */
    private Integer classifyId;

    /**
     * 商品库存
     */
    private Integer goodsInventory;

    /**
     * 商品销量
     */
    private Integer goodsSales;

    /**
     * 商品状态
     */
    private String goodsStatus;


}
