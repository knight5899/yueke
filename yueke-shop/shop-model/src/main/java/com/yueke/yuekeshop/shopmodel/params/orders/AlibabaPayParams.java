package com.yueke.yuekeshop.shopmodel.params.orders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlibabaPayParams {

    private  Integer	ordersId;

    private  String 	ordersNum;

    private  double 	ordersSumPrice;

}
