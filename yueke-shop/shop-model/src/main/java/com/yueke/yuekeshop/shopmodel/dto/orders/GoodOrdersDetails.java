package com.yueke.yuekeshop.shopmodel.dto.orders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodOrdersDetails{

    /**
     * 订单ID
     */
    private Integer ordersId;

    /**
     * 订单编号
     */
    private String ordersNum;

    /**
     * 订单提交时间
     */
    private long ordersStartTime;

    /**
     * 订单总价
     */
    private double ordersSumPrice;

    /**
     * 订单状态
     */
    private String ordersStatus;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 订单项List集合
     */
    private  List<GoodsOrdersItemDto> goodsOrdersItemDtoList;

}
