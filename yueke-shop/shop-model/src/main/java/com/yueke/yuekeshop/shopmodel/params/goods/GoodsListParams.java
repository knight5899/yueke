package com.yueke.yuekeshop.shopmodel.params.goods;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * redis 模糊查询方法参数
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoodsListParams {

  /**
   * 起始页
   */
  @NotNull
  private Integer pageNum;

  /**
   * 页面记录数
   */
  @NotNull
  private Integer pageSize;

  /**
   * 商品分类id
   */
  private Integer classifyId;

  /**
   * 营地id
   */
  private Integer campsiteId;

  /**
   * 商品名称
   */
  private String goodsName;

  /**
   * 商品状态
   */
  @NotNull
  private String goodsStatus;


}
