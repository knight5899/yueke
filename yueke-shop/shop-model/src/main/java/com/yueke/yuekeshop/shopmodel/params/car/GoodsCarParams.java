package com.yueke.yuekeshop.shopmodel.params.car;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodsCarParams {

  /**
   * 商品id
   */
  @NotNull
  private  Integer	goodsId;

  /**
   * 购买商品数量
   */
  @NotNull
  private  Integer	goodsOrdersCount;

}
