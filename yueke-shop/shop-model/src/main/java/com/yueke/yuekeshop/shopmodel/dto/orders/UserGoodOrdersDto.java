package com.yueke.yuekeshop.shopmodel.dto.orders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserGoodOrdersDto{

    /**
     * 订单id
     */
    private Integer ordersId;

    /**
     * 订单编号
     */
    private String ordersNum;

    /**
     * 订单开始时间
     */
    private long ordersStartTime;

    /**
     * 订单总价
     */
    private double ordersSumPrice;

    /**
     * 订单状态
     */
    private String ordersStatus;

    /**
     * 订单客户姓名
     */
    private String userName;




}
