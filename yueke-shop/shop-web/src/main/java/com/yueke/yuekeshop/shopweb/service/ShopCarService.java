package com.yueke.yuekeshop.shopweb.service;


import com.yueke.yuekeshop.shopmodel.dto.orders.UserGoodOrdersDto;
import com.yueke.yuekeshop.shopmodel.params.car.GoodsCarParams;

public interface ShopCarService {

    /**
     * 添加购物车
     * @param goodsCarParams
     */
    void addOrdersCar(GoodsCarParams goodsCarParams);

    /**
     * 删除购车商品
     * @param goodsId
     */
    void deleteOrdersCar(Integer goodsId);

    /**
     * 自增购物车商品数量
     * @param goodsId
     */
    void incrementOrdersCarCount(Integer goodsId);

    /**
     * 自减购物车商品数量
     * @param goodsId
     */
    void decrementOrdersCarCount(Integer goodsId);
    /**
     * 立即购买
     * @param goodsCarParams
     * @return
     */
    UserGoodOrdersDto payOrdersCar(GoodsCarParams goodsCarParams);
}
