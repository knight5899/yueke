package com.yueke.yuekeshop.shopweb.controller;




import com.yueke.common.core.dto.ResultDto;
import com.yueke.yuekeshop.shopmodel.dto.orders.GoodOrdersDetails;
import com.yueke.yuekeshop.shopmodel.dto.orders.UserGoodOrdersDto;
import com.yueke.yuekeshop.shopmodel.params.orders.AlibabaPayParams;
import com.yueke.yuekeshop.shopmodel.params.orders.OrdersListParams;
import com.yueke.yuekeshop.shopmodel.params.orders.UserOrdersStatus;
import com.yueke.yuekeshop.shopweb.service.ShopOrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop")
@Slf4j
public class ShopOrdersController {

    @Autowired
    private ShopOrdersService shopOrdersService;



    /**
     * 添加商品订单
     * @param goodsIds
     * @return
     */
    @GetMapping("orders/addGoodsOrder")
    public ResultDto addGoodsOrder(List<String> goodsIds){
        log.info("请求参数{goodsIds}:"+goodsIds);
       UserGoodOrdersDto userGoodOrdersDto = shopOrdersService.addGoodsOrder(goodsIds);
        return ResultDto.builder().code(200).msg("新增付款订单").data(userGoodOrdersDto).build();
    }


    /**
     * 关闭订单
     * @param ordersId
     * @return
     */
    @GetMapping("orders/updateGoodsOrderStatus/{ordersId}")
    public ResultDto updateGoodsOrderStatus(@PathVariable("ordersId")Integer ordersId){
        log.info("请求参数{ordersId}:"+ordersId);
        shopOrdersService.updateGoodsOrderStatus(ordersId);
        return ResultDto.builder().code(200).msg("订单关闭成功:"+ordersId).data(true).build();
    }


    /**
     * 支付功能
     * @param alibabaPayParams
     * @return
     */
    @PostMapping("orders/alibabaPayOrder")
    public ResultDto alibabaPayOrder(@RequestBody AlibabaPayParams alibabaPayParams){
        log.info("请求参数{alibabaPayParams}:"+alibabaPayParams);
        shopOrdersService.alibabaPayOrder(alibabaPayParams);
        return ResultDto.builder().code(200).msg("订单付款成功").data(true).build();
    }


    /**
     * 查询订单列表(可模糊)
     * @param ordersListParams
     * @return
     */
    @PostMapping("orders/selectOrdersList")
    public ResultDto selectOrdersList(@RequestBody OrdersListParams ordersListParams){
        log.info("请求参数{alibabaPayParams}:"+ordersListParams);
       List<UserGoodOrdersDto> userGoodOrdersDtoList = shopOrdersService.selectOrdersList(ordersListParams);
        return ResultDto.builder().code(200).msg("查询订单列表成功").data(userGoodOrdersDtoList).build();
    }


    /**
     * 查询订单详情
     * @param ordersId
     * @return
     */
    @GetMapping("orders/selectOrdersById/{ordersId}")
    public ResultDto selectOrdersById(@PathVariable("ordersId")Integer ordersId){
        log.info("请求参数{ordersId}:"+ordersId);
        GoodOrdersDetails goodOrdersDetails=shopOrdersService.selectOrdersById(ordersId);
        return ResultDto.builder().code(200).msg("查询订单详情成功{ordersId}:"+ordersId).data(goodOrdersDetails).build();
    }


    /**
     * 用户查询个人订单(可模糊)
     * @param userOrdersStatus
     * @return
     */
    @PostMapping("orders/selectUserOrdersId")
    public ResultDto selectUserOrdersId(@RequestBody UserOrdersStatus userOrdersStatus){
        log.info("请求参数{userOrdersStatus}:"+userOrdersStatus);
        List<GoodOrdersDetails> goodOrdersDetailsList =shopOrdersService.selectUserOrdersId(userOrdersStatus);
        return ResultDto.builder().code(200).msg("用户查询订单成功").data(goodOrdersDetailsList).build();
    }

}
