package com.yueke.yuekeshop.shopweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.shop.YuekeShopOrders;

public interface ShopOrdersMapper extends BaseMapper<YuekeShopOrders> {
}
