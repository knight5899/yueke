package com.yueke.yuekeshop.shopweb.service.impl;



import com.yueke.yuekeshop.shopweb.service.ShopInformService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ShopInformServiceImpl implements ShopInformService {


    /**
     * 消息通知
     */
    @Override
    public void shopInform() {

    }
}
