package com.yueke.yuekeshop.shopweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.shop.YuekeShopClassify;

public interface ShopClassifyMapper extends BaseMapper<YuekeShopClassify> {
}
