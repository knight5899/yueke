package com.yueke.yuekeshop.shopweb.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.yueke.common.core")
public class CommConfig {
}
