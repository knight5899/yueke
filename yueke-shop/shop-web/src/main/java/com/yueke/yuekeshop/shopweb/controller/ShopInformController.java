package com.yueke.yuekeshop.shopweb.controller;




import com.yueke.common.core.dto.ResultDto;
import com.yueke.yuekeshop.shopweb.service.ShopInformService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/shop")
@Slf4j
public class ShopInformController {

    @Autowired
    private ShopInformService shopInformService;


    /**
     * 消息通知
     * @return
     */
    @PostMapping("inform/shopInform")
    public ResultDto shopInform(){
        log.info("请求参数{}:"+"无");
         shopInformService.shopInform();
        return ResultDto.builder().code(200).msg("有新消息").data(true).build();
    }
}
