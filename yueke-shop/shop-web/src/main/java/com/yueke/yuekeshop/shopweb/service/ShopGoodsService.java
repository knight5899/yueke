package com.yueke.yuekeshop.shopweb.service;





import com.yueke.yuekeshop.shopmodel.dto.goods.ShopGoodsDto;
import com.yueke.yuekeshop.shopmodel.params.goods.GoodsIdOrNumParams;
import com.yueke.yuekeshop.shopmodel.params.goods.GoodsListParams;
import com.yueke.yuekeshop.shopmodel.params.goods.ShopGoodsParams;

import java.util.List;

public interface ShopGoodsService {

    /**
     * 查询商品详情
     * @param goodsId
     * @return
     */
    ShopGoodsDto selectGoodsById(Integer goodsId);

    /**
     * redis 测试
     * @param admin
     * @return
     */
    String createRedisKey(String admin);

    /**
     * 根据参数分类名称,新增一条分类记录
     * @param classifyName
     */
    void addClassify(String classifyName);

    /**
     * 添加商品入库
     * @param shopGoodsParams
     */
    void addGoody(ShopGoodsParams shopGoodsParams);

    /**
     * 商品上架(可批量)
     * @param goodsIdOrNumParamsList
     */
    void onGoodsSheIfList(List<GoodsIdOrNumParams> goodsIdOrNumParamsList);


    /**
     * 商品下架(可批量)
     * @param goodsIdOrNumParamsList
     */
    void downGoodsSheIfList(List<GoodsIdOrNumParams> goodsIdOrNumParamsList);

    /**
     * 未上架商品删除(可批量)
     * @param goodsIdOrNumParamsList
     */
    void deleteGoodsIds(List<GoodsIdOrNumParams> goodsIdOrNumParamsList);

    /**
     * 后台查询商品(可模糊)
     * @param goodsListParams
     */
    List<ShopGoodsDto> selectOnGoodsList(GoodsListParams goodsListParams);

    /**
     * 修改商品详情
     * @param shopGoodsParams
     * @return
     */
    void updateGoodsOne(ShopGoodsParams shopGoodsParams);
}
