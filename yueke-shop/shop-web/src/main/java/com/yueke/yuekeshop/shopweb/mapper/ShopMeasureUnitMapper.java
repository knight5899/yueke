package com.yueke.yuekeshop.shopweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.shop.YuekeShopMeasureUnit;

public interface ShopMeasureUnitMapper extends BaseMapper<YuekeShopMeasureUnit> {
}
