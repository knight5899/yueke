package com.yueke.yuekeshop.shopweb.controller;




import com.yueke.common.core.dto.ResultDto;
import com.yueke.yuekeshop.shopmodel.dto.orders.UserGoodOrdersDto;
import com.yueke.yuekeshop.shopmodel.params.car.GoodsCarParams;
import com.yueke.yuekeshop.shopweb.service.ShopCarService;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shop")
@Slf4j
@Builder
public class ShopCarController {

    @Autowired
    private ShopCarService shopCarService;



    /**
     * 添加购物车
     * @param goodsCarParams
     * @return
     */
    @PostMapping("car/addOrdersCar")
    public ResultDto addOrdersCar(GoodsCarParams goodsCarParams){
        log.info("请求参数{goodsCarParams}:"+goodsCarParams);
        shopCarService.addOrdersCar(goodsCarParams);
        return ResultDto.builder().code(200).msg("商品添加购物车成功").data(true).build();
    }




    /**
     * 删除购物车商品
     * @param goodsId
     * @return
     */
    @DeleteMapping("car/deleteOrdersCar/{goodsId}")
    public ResultDto deleteOrdersCar(@PathVariable("goodsId") Integer goodsId){
        log.info("请求参数{goodsId}:"+goodsId);
        shopCarService.deleteOrdersCar(goodsId);
        return ResultDto.builder().code(200).msg("删除购物车商品成功{goodsId}:"+goodsId).data(true).build();
    }



    /**
     * 自增购物车商品数量
     * @param goodsId
     * @return
     */
    @DeleteMapping("car/incrementOrdersCarCount/{id}")
    public ResultDto incrementOrdersCarCount(@PathVariable("id") Integer goodsId){
        log.info("请求参数{goodsId}:"+goodsId);
        shopCarService.incrementOrdersCarCount(goodsId);
        return ResultDto.builder().code(200).msg("自增购物车商品数量成功{goodsId}:"+goodsId).data(true).build();
    }

    /**
     * 自减购物车商品数量
     * @param goodsId
     * @return
     */
    @DeleteMapping("car/decrementOrdersCarCount/{id}")
    public ResultDto decrementOrdersCarCount(@PathVariable("id") Integer goodsId){
        log.info("请求参数{goodsId}:"+goodsId);
        shopCarService.decrementOrdersCarCount(goodsId);
        return ResultDto.builder().code(200).msg("自减购物车商品数量成功{goodsId}:"+goodsId).data(true).build();
    }


    /**
     * 立即购买
     * @param goodsCarParams
     * @return
     */
    @PostMapping("car/payOrdersCar")
    public ResultDto payOrdersCar(GoodsCarParams goodsCarParams){
        log.info("请求参数{goodsCarParams}:"+goodsCarParams);
       UserGoodOrdersDto userGoodOrdersDto = shopCarService.payOrdersCar(goodsCarParams);
        return ResultDto.builder().code(200).msg("新增付款订单").data(true).build();
    }


}
