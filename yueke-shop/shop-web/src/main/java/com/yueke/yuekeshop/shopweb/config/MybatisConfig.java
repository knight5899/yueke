package com.yueke.yuekeshop.shopweb.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.yueke.yuekeshop.shopweb.mapper")
public class MybatisConfig {


}
