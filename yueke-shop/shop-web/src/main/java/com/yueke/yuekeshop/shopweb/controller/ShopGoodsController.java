package com.yueke.yuekeshop.shopweb.controller;






import com.yueke.common.core.dto.ResultDto;
import com.yueke.yuekeshop.shopmodel.dto.goods.ShopGoodsDto;
import com.yueke.yuekeshop.shopmodel.params.goods.GoodsIdOrNumParams;
import com.yueke.yuekeshop.shopmodel.params.goods.GoodsListParams;
import com.yueke.yuekeshop.shopmodel.params.goods.ShopGoodsParams;
import com.yueke.yuekeshop.shopweb.service.ShopGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/shop")
@Slf4j
public class ShopGoodsController {

    @Autowired
    private ShopGoodsService shopgoodsService;


//    /**
//     * OSS 服务
//     */
//    @Autowired
//    private OssService ossService;



    //    /**
//     * 测试接口  测试图片上传 是否可用
//     * @param file
//     * @return
//     */
//    @PostMapping("uploadPicGoods")
//    public ResultDto uploadPicGoods(MultipartFile file){
//        System.out.println(ossService);
//        log.info("请求参数{file}:"+file);
//        String url = ossService.uploadPicAvatar(file);
//        return ResultDto.builder().code(200).msg("执行成功").data(url).build();
//    }


    /**
     *  根据参数分类名称,新增一条分类记录
     * @param classifyName
     * @return
     */
    @PostMapping("classify/addClassify")
    public ResultDto addClassify(String classifyName){
        log.info("请求参数{classifyName}:"+classifyName);
        shopgoodsService.addClassify(classifyName);
        return ResultDto.builder().code(200).msg("新增成功").data(true).build();
    }

    /**
     * 添加商品入库
     * @param shopGoodsParams
     * @return
     */
    @PostMapping("goods/addGoody")
    public ResultDto addGoody(@RequestBody @Valid ShopGoodsParams shopGoodsParams){
        long startTime = System.currentTimeMillis();
        log.info("请求参数{shopGoodsParams}:"+shopGoodsParams);
        shopgoodsService.addGoody(shopGoodsParams);
        long endTime = System.currentTimeMillis();
        long elapsedTime  = startTime - endTime;
        return ResultDto.builder().code(200).msg("(总耗时:"+elapsedTime+")新增商品成功").data(true).build();
    }


    /**
     * 商品上架(可批量)
     * @param goodsIdOrNumParamsList
     * @return
     */
    @PostMapping("goods/onGoodsSheIfList")
    public ResultDto onGoodsSheIf(@RequestBody @Valid List<GoodsIdOrNumParams> goodsIdOrNumParamsList){
        log.info("请求参数{goodsIdOrNumParamsList}:"+goodsIdOrNumParamsList);
        shopgoodsService.onGoodsSheIfList (goodsIdOrNumParamsList);
        return ResultDto.builder().code(200).msg("商品上架成功").data(true).build();
    }

    /**
     * 商品下架(可批量)
     * @param goodsIdOrNumParamsList
     * @return
     */
    @PostMapping("goods/downGoodsSheIfList")
    public ResultDto downGoodsSheIfList(@RequestBody @Valid List<GoodsIdOrNumParams> goodsIdOrNumParamsList){
        log.info("请求参数{goodsIdOrNumParamsList}:"+goodsIdOrNumParamsList);
        shopgoodsService.downGoodsSheIfList(goodsIdOrNumParamsList);
        return ResultDto.builder().code(200).msg("商品下架成功").data(true).build();
    }

    /**
     * 删除未上架商品(可批量)
     * @param goodsIdOrNumParamsList
     * @return
     */
    @DeleteMapping("goods/deleteGoodsIds")
    public ResultDto deleteGoodsIds(@RequestBody @Valid List<GoodsIdOrNumParams> goodsIdOrNumParamsList){
        log.info("请求参数{goodsId}:"+goodsIdOrNumParamsList);
        shopgoodsService.deleteGoodsIds(goodsIdOrNumParamsList);
        return ResultDto.builder().code(200).msg("已删除未上架商品").data(true).build();
    }


    /**
     * 后台查询商品(可模糊)
     * @param goodsListParams
     * @return
     */
    @PostMapping("goods/selectOnGoodsList")
    public ResultDto selectOnGoodsList(@Valid GoodsListParams goodsListParams){
        long startTime = System.currentTimeMillis();
        log.info("请求参数{goodsListParams}:"+goodsListParams);
        List<ShopGoodsDto> shopGoodsDtos = shopgoodsService.selectOnGoodsList(goodsListParams);
        long endTime = System.currentTimeMillis();
        long elapsedTime  = startTime - endTime;
        return ResultDto.builder().code(200).msg("(redis模糊查询总耗时"+elapsedTime+"):商品查询成功").data(shopGoodsDtos).build();
    }

    /**
     * 查询商品详情
     * @param goodsId
     * @return
     */
    @GetMapping("goods/selectGoodsById/{goodsId}")
    public ResultDto selectGoodsById(@PathVariable("goodsId")Integer goodsId){
        log.info("请求参数{goodsId}:"+goodsId);
        ShopGoodsDto shopGoodsDto = shopgoodsService.selectGoodsById(goodsId);
        return ResultDto.builder().code(200).msg("商品详情查询成功{goodsId}:"+goodsId).data(shopGoodsDto).build();
    }


    /**
     * 修改商品详情
     * @param shopGoodsParams
     * @return
     */
    @PostMapping("goods/updateGoodsOne")
    public ResultDto updateGoodsOne(@RequestBody @Valid ShopGoodsParams shopGoodsParams){
        log.info("请求参数{shopGoodsParams}:"+shopGoodsParams);
        shopgoodsService.updateGoodsOne(shopGoodsParams);
        return ResultDto.builder().code(200).msg("商品详情修改成功{goodsId}:"+shopGoodsParams.getGoodsId()).data(true).build();
    }
}
