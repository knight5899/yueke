package com.yueke.yuekeshop.shopweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.shop.YuekeShopCoupon;

public interface ShopCouponMapper extends BaseMapper<YuekeShopCoupon> {
}
