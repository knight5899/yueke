package com.yueke.yuekeshop.shopweb.service;





import com.yueke.yuekeshop.shopmodel.dto.orders.GoodOrdersDetails;
import com.yueke.yuekeshop.shopmodel.dto.orders.UserGoodOrdersDto;
import com.yueke.yuekeshop.shopmodel.params.orders.AlibabaPayParams;
import com.yueke.yuekeshop.shopmodel.params.orders.OrdersListParams;
import com.yueke.yuekeshop.shopmodel.params.orders.UserOrdersStatus;

import java.util.List;

public interface ShopOrdersService {

    /**
     * 新增订单
     * @param goodsIds
     * @return
     */
    UserGoodOrdersDto addGoodsOrder(List<String> goodsIds);

    /**
     * 关闭订单
     * @param ordersId
     */
    void updateGoodsOrderStatus(Integer ordersId);

    /**
     * 支付功能
     * @param alibabaPayParams
     */
    void alibabaPayOrder(AlibabaPayParams alibabaPayParams);

    /**
     * 查询订单列表(可模糊)
     * @param ordersListParams
     * @return
     */
    List<UserGoodOrdersDto> selectOrdersList(OrdersListParams ordersListParams);


    /**
     * 查询商品订单详情
     * @param ordersId
     * @return
     */
    GoodOrdersDetails selectOrdersById(Integer ordersId);

    /**
     * 用户查询个人订单
     * @param userOrdersStatus
     * @return
     */
    List<GoodOrdersDetails> selectUserOrdersId(UserOrdersStatus userOrdersStatus);
}
