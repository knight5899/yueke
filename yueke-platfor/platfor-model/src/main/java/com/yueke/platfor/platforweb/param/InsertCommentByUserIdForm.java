package com.yueke.platfor.platforweb.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/15 11:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InsertCommentByUserIdForm {

    private Integer commentnotesId;

    private Integer commentnotesUserid;

    private Date commentnotesTime;

    private String commentnotesInformation;

    private String commentnotesState;

}
