package com.yueke.platfor.platforweb.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/15 11:50
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindCommentnotesForm {

    private Integer commentnotesUserid;

    private Date commentnotesStartTime;//时间区间头(大于)

    private Date commentnotesEndTime;//时间区间尾(小于)

    private String commentnotesInformation; //评论内容(模糊)

    private String commentnotesState;

}
