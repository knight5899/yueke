package com.yueke.platfor.platforweb.param;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/16 9:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateMaintenanceRvByIdForm {
    private Integer rvmaintenanceId;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String rvmaintenanceRvfault;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Date rvmaintenanceCompletiontime;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String rvmaintenanceProgress;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String rvmaintenanceState;
}
