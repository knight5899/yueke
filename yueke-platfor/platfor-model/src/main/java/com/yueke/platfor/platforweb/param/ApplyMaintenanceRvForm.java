package com.yueke.platfor.platforweb.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/15 19:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApplyMaintenanceRvForm {

    private Integer rvmaintenanceRvid;

    private String rvmaintenanceRvfault;

    private Date rvmaintenanceCompletiontime;



}
