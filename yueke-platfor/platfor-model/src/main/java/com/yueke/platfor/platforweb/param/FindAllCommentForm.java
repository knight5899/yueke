package com.yueke.platfor.platforweb.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/15 15:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindAllCommentForm {
    private Integer pageNum;
    private Integer pageSize;
    private Integer commentmanagementUserid;
    private String commentmanagementInformation;
    private String commentmanagementContent;
    private Date commentmanagementStartTime;
    private Date commentmanagementEndTime;
    private String commentmanagementState;
}
