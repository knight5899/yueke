package com.yueke.platfor.platforweb.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 胡八一
 * @date 2020/10/15 14:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetCommentForm {
    private Integer commentnotesId;
    private String commentmanagementContent;

}
