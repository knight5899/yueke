package com.yueke.platfor.platforweb.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/15 18:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindAllNoticeForm {
    private Integer pageNum;
    private Integer pageSize;

    private String noticeTheme;

    private String noticeDepartment;

    private String noticeContent;

    private Date noticeStartTime;

    private Date noticeEndTime;
}
