package com.yueke.platfor.platforweb.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/15 20:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindAllMaintenanceRvForm {
    private Integer pageNum;
    private Integer pageSize;

    private Integer rvmaintenanceId;

    private String rvmaintenanceRvfault;

    private Date rvmaintenanceStartTime;

    private Date rvmaintenanceEndTime;

    private Date rvmaintenanceCompletionStartTime;

    private Date rvmaintenanceCompletionEndTime;

    private String rvmaintenanceProgress;

    private String rvmaintenanceState;


}
