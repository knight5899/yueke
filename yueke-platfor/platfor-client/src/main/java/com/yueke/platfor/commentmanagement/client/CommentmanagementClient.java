package com.yueke.platfor.commentmanagement.client;

import com.yueke.common.core.dto.ResultDto;
import com.yueke.platfor.platforweb.param.GetCommentForm;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 胡八一
 * @date 2020/10/16 11:02
 */
@FeignClient("platfor-server")
@RequestMapping("/platfor/commentmanagement")
public interface CommentmanagementClient {
    /**
     * 评论回复
     * @param form
     * @return
     */
    @GetMapping("getComment")
    public ResultDto getComment(@RequestBody GetCommentForm form);

}
