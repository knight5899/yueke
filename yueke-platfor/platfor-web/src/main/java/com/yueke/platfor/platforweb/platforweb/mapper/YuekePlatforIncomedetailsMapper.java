package com.yueke.platfor.platforweb.platforweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.platfor.YuekePlatforIncomedetails;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforIncomedetailsMapper extends BaseMapper<YuekePlatforIncomedetails> {

}
