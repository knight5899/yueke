package com.yueke.platfor.platforweb.platforweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.platfor.YuekePlatforIncomedetails;
import com.yueke.platfor.platforweb.platforweb.mapper.YuekePlatforIncomedetailsMapper;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforIncomedetailsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekePlatforIncomedetailsServiceImpl extends ServiceImpl<YuekePlatforIncomedetailsMapper, YuekePlatforIncomedetails> implements YuekePlatforIncomedetailsService {

}
