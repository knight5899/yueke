package com.yueke.platfor.platforweb.platforweb.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentnotes;
import com.yueke.common.core.dto.ResultDto;
import com.yueke.platfor.platforweb.param.FindCommentnotesForm;
import com.yueke.platfor.platforweb.param.InsertCommentByUserIdForm;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforCommentnotesService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 * 评论管理
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("/platfor/commentnotes")
@Slf4j
public class YuekePlatforCommentnotesController {

    @Resource
    private YuekePlatforCommentnotesService yuekePlatforCommentnotesService;

    /**
     * 评论的新增
     * @param form 参数对象
    * @return
     */
    @PutMapping("insertCommentByUserId")
    @ApiOperation("评论的新增")
    public ResultDto insertCommentByUserId(@RequestBody InsertCommentByUserIdForm form){
        log.info("参数对象form:{}",form);
        form.setCommentnotesState("正常");
        YuekePlatforCommentnotes yuekePlatforCommentnotes = new YuekePlatforCommentnotes();
        BeanUtils.copyProperties(form, yuekePlatforCommentnotes);
        boolean b = yuekePlatforCommentnotesService.save(yuekePlatforCommentnotes);
        if(b) return ResultDto.success().setMsg("评论新增成功").setCode(200).setData(yuekePlatforCommentnotes);
        return ResultDto.success().setMsg("评论失败").setCode(400);
    }

    /**
     * 评论查询
     * @param form
     * @return
     */
    @GetMapping("findCommentnotes")
    @ApiOperation("评论查询")
    public ResultDto findCommentnotes(@RequestBody FindCommentnotesForm form){
        log.info("参数对象form:{}",form);
        List<YuekePlatforCommentnotes> list = yuekePlatforCommentnotesService.findCommentnotes(form);
        if(list != null)return ResultDto.success().setMsg("查询成功").setCode(200).setData(list);
        return ResultDto.success().setMsg("查询失败").setCode(400);
    }




}

