package com.yueke.platfor.platforweb.platforweb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.yueke.platfor.platforweb.platforweb.mapper")
@EnableFeignClients
@ComponentScan("com.yueke.yuekecommon.yuekeweb.config")
@ComponentScan("com.yueke.platfor.platforweb.platforweb.controller")
public class PlatforWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlatforWebApplication.class, args);
    }

}
