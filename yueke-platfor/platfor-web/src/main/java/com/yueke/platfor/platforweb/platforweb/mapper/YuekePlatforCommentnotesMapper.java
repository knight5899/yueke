package com.yueke.platfor.platforweb.platforweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentnotes;
import com.yueke.platfor.platforweb.param.FindCommentnotesForm;


import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforCommentnotesMapper extends BaseMapper<YuekePlatforCommentnotes> {

    List<YuekePlatforCommentnotes> findCommentnotes(FindCommentnotesForm form);
}
