package com.yueke.platfor.platforweb.platforweb.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentnotes;
import com.yueke.common.core.dto.ResultDto;

import com.yueke.platfor.platforweb.param.FindAllCommentForm;
import com.yueke.platfor.platforweb.param.GetCommentForm;
import com.yueke.platfor.platforweb.param.UpdateCommentForm;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforCommentmanagementService;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforCommentnotesService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("/platfor/commentmanagement")
@Slf4j
public class YuekePlatforCommentmanagementController {

    @Resource
    private YuekePlatforCommentmanagementService yuekePlatforCommentmanagementService;
    @Resource
    private YuekePlatforCommentnotesService yuekePlatforCommentnotesService;

    /**
     * 评论回复
     * @param form
     * @return
     */
    @GetMapping("getComment")
    @ApiOperation("评论回复")
    public ResultDto getComment(@RequestBody GetCommentForm form){
        log.info("参数对象form:{}",form);
        YuekePlatforCommentnotes byId = yuekePlatforCommentnotesService.getById(form.getCommentnotesId());
        YuekePlatforCommentmanagement yuekePlatforCommentmanagement = new YuekePlatforCommentmanagement();
        yuekePlatforCommentmanagement.setCommentmanagementTime(new Date());
        yuekePlatforCommentmanagement.setCommentmanagementContent(form.getCommentmanagementContent());
        yuekePlatforCommentmanagement.setCommentmanagementState("正常使用");
        yuekePlatforCommentmanagement.setCommentmanagementUserid(byId.getCommentnotesUserid());
        yuekePlatforCommentmanagement.setCommentmanagementInformation(byId.getCommentnotesInformation());
        boolean b = yuekePlatforCommentmanagementService.save(yuekePlatforCommentmanagement);
        if(b) return ResultDto.success().setMsg("评论回复成功").setCode(200).setData(yuekePlatforCommentmanagement);
        return ResultDto.success().setMsg("评论回复失败").setCode(400);
    }


    /**
     * 查询所有评论回复
     * @param form
     * @return
     */
    @GetMapping("findAllComment")
    @ApiOperation("查询所有评论回复")
    public ResultDto findAllComment(@RequestBody FindAllCommentForm form){
        log.info("参数对象form:{}",form);
        QueryWrapper<YuekePlatforCommentmanagement> wrapper = new QueryWrapper<>();
        if(form.getCommentmanagementUserid() != null && form.getCommentmanagementUserid()+"" != "")
            wrapper.eq("commentmanagement_userId",form.getCommentmanagementUserid());
        if(form.getCommentmanagementStartTime() != null)
            wrapper.gt("commentmanagement_time",form.getCommentmanagementStartTime());
        if(form.getCommentmanagementEndTime() != null)
            wrapper.lt("commentmanagement_time",form.getCommentmanagementEndTime());
        if(form.getCommentmanagementInformation() != null && form.getCommentmanagementInformation() != "")
            wrapper.like("commentmanagement_information",form.getCommentmanagementInformation());
        if(form.getCommentmanagementContent() != null && form.getCommentmanagementContent() != "")
            wrapper.like("commentmanagement_content",form.getCommentmanagementContent());
        if(form.getCommentmanagementState() != null && form.getCommentmanagementState() != "")
            wrapper.eq("commentmanagement_state",form.getCommentmanagementState());
        Page<YuekePlatforCommentmanagement> page = new Page<>(form.getPageNum(),form.getPageSize());
        IPage<YuekePlatforCommentmanagement> byPage = yuekePlatforCommentmanagementService.selectAllByPage(page, wrapper);
        return ResultDto.success().setMsg("查询成功").setCode(200).setData(byPage);
    }

    /**
     * 评论信息修改
     * @param form
     * @return
     */
    @PostMapping("updateComment")
    @ApiOperation("评论信息修改")
    public ResultDto updateComment(@RequestBody UpdateCommentForm form){
        log.info("参数对象form:{}",form);
        YuekePlatforCommentmanagement yuekePlatforCommentmanagement = new YuekePlatforCommentmanagement();
        yuekePlatforCommentmanagement.setCommentmanagementId(form.getCommentmanagementId());
        yuekePlatforCommentmanagement.setCommentmanagementContent(form.getCommentmanagementContent());
        yuekePlatforCommentmanagement.setCommentmanagementState(form.getCommentmanagementState());
        boolean b = yuekePlatforCommentmanagementService.updateById(yuekePlatforCommentmanagement);
        if(b)return ResultDto.success().setMsg("更新回复成功").setCode(200);
        return ResultDto.success().setMsg("更新回复失败").setCode(400);
    }






















    /*@GetMapping("demo")
    public ResultDto demo(){
        List<YuekePlatforCommentmanagement> list = yuekePlatforCommentmanagementService.list();
        return ResultDto.success().setMsg("成功查询").setCode(200).setData(list);
    }*/

    /*@GetMapping("demo2")
    public ResultDto demo2(){
        Page<YuekePlatforCommentmanagement> page = new Page<>(2,5);
        IPage<YuekePlatforCommentmanagement> list = yuekePlatforCommentmanagementService.selectAllByPage(page);
        return ResultDto.success().setData(list);
    }*/

    /*@GetMapping("demo3")
    public ResultDto demo3(){
        List<YuekePlatforCommentmanagement> list = yuekePlatforCommentmanagementService.selectAll();
        return ResultDto.success().setData(list);
    }*/


}

