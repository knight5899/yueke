package com.yueke.platfor.platforweb.platforweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;
import com.yueke.platfor.platforweb.param.FindAllCommentForm;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforCommentmanagementMapper extends BaseMapper<YuekePlatforCommentmanagement> {

    /**
     * @param page 分页对象,xml中可以从里面进行取值,传递参数 Page 即自动分页,必须放在第一位(你可以继承Page实现自己的分页对象)
     * @return 分页对象
     */
    IPage<YuekePlatforCommentmanagement> selectAllByPage(Page<YuekePlatforCommentmanagement> page, FindAllCommentForm form);

    List<YuekePlatforCommentmanagement> selectAll();
}
