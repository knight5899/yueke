package com.yueke.platfor.platforweb.platforweb.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("/platfor/incomedetails")
public class YuekePlatforIncomedetailsController {

}

