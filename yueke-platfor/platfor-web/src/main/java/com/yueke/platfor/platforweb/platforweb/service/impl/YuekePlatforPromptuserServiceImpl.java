package com.yueke.platfor.platforweb.platforweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.platfor.YuekePlatforPromptuser;
import com.yueke.platfor.platforweb.platforweb.mapper.YuekePlatforPromptuserMapper;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforPromptuserService;
import org.springframework.stereotype.Service;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekePlatforPromptuserServiceImpl extends ServiceImpl<YuekePlatforPromptuserMapper, YuekePlatforPromptuser> implements YuekePlatforPromptuserService {

}
