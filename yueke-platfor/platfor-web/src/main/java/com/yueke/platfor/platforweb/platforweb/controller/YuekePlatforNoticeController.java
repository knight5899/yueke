package com.yueke.platfor.platforweb.platforweb.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;
import com.yueke.common.core.domain.platfor.YuekePlatforNotice;
import com.yueke.common.core.dto.ResultDto;
import com.yueke.platfor.platforweb.param.ApplicatioNoticeForm;
import com.yueke.platfor.platforweb.param.ChangeNoticeStateForm;
import com.yueke.platfor.platforweb.param.FindAllNoticeForm;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforNoticeService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("/platfor/notice")
@Slf4j
public class YuekePlatforNoticeController {

    @Resource
    private YuekePlatforNoticeService yuekePlatforNoticeService;

    /**
     * 公告申请
     * @param form
     * @return
     */
    @PutMapping("applicatioNotice")
    @ApiOperation("公告申请")
    public ResultDto applicatioNotice(@RequestBody ApplicatioNoticeForm form){
        log.info("参数对象form:{}",form);
        YuekePlatforNotice yuekePlatforNotice = new YuekePlatforNotice();
        BeanUtils.copyProperties(form, yuekePlatforNotice);
        yuekePlatforNotice.setNoticeState("待使用");
        boolean b = yuekePlatforNoticeService.save(yuekePlatforNotice);
        if(b)return ResultDto.success().setMsg("公告申请成功").setCode(200);
        return ResultDto.success().setMsg("公告申请失败").setCode(400);
    }

    /**
     * 公告状态
     * @param form
     * @return
     */
    @PostMapping("changeNoticeState")
    @ApiOperation("公告状态")
    public ResultDto changeNoticeState(@RequestBody ChangeNoticeStateForm form){
        log.info("参数对象form:{}",form);
        YuekePlatforNotice yuekePlatforNotice = new YuekePlatforNotice();
        BeanUtils.copyProperties(form, yuekePlatforNotice);
        boolean b = yuekePlatforNoticeService.updateById(yuekePlatforNotice);
        if(b)return ResultDto.success().setMsg("修改状态成功").setCode(200);
        return ResultDto.success().setMsg("修改状态失败").setCode(200);
    }

    /**
     * 公告发布
     * @param form
     * @return
     */
    @GetMapping("findAllNotice")
    @ApiOperation("公告发布")
    public ResultDto findAllNotice(@RequestBody FindAllNoticeForm form){
        log.info("参数对象form:{}",form);
        QueryWrapper<YuekePlatforNotice> wrapper = new QueryWrapper<>();
        if(form.getNoticeTheme() != null && form.getNoticeTheme() != "")
            wrapper.like("notice_theme",form.getNoticeTheme());
        if(form.getNoticeDepartment() != null && form.getNoticeDepartment() != "")
            wrapper.like("notice_department",form.getNoticeDepartment());
        if(form.getNoticeContent() != null && form.getNoticeContent() != "")
            wrapper.like("notice_content",form.getNoticeContent());
        if(form.getNoticeStartTime() != null)
            wrapper.gt("notice_time",form.getNoticeStartTime());
        if(form.getNoticeEndTime() != null)
            wrapper.lt("notice_time",form.getNoticeEndTime());
        Page<YuekePlatforNotice> page = new Page<>(form.getPageNum(),form.getPageSize());
        IPage<YuekePlatforNotice> byPage = yuekePlatforNoticeService.selectAllByPage(page, wrapper);
        return ResultDto.success().setMsg("查询成功").setCode(200).setData(byPage);
    }


}

