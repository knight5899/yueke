package com.yueke.platfor.platforweb.platforweb.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author 胡八一
 * @date 2020/10/15 19:51
 */
public class MyTimeUtil {

    /**
          * 当前日期加上天数后的日期
          * @param num 为增加的天数
          * @return
           */
     public static Date addDays(int num){
                Date d = new Date();
               SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               String currdate = format.format(d);
                Calendar ca = Calendar.getInstance();
                ca.add(Calendar.DATE, num);// num为增加的天数，可以改变的
                d = ca.getTime();
                return d;
     }

}
