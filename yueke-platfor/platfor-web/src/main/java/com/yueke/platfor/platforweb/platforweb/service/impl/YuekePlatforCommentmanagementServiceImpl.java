package com.yueke.platfor.platforweb.platforweb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;
import com.yueke.platfor.platforweb.platforweb.mapper.YuekePlatforCommentmanagementMapper;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforCommentmanagementService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekePlatforCommentmanagementServiceImpl extends ServiceImpl<YuekePlatforCommentmanagementMapper, YuekePlatforCommentmanagement> implements YuekePlatforCommentmanagementService {
    @Resource
    private YuekePlatforCommentmanagementMapper yuekePlatforCommentmanagementMapper;

    @Override
    public IPage<YuekePlatforCommentmanagement> selectAllByPage(Page<YuekePlatforCommentmanagement> page, QueryWrapper<YuekePlatforCommentmanagement> wrapper) {
        // 不进行 count sql 优化，解决 MP 无法自动优化 SQL 问题，这时候你需要自己查询 count 部分
        page.setOptimizeCountSql(false);
        // 当 total 为小于 0 或者设置 setSearchCount(false) 分页插件不会进行 count 查询
        // 要点!! 分页返回的对象与传入的对象是同一个
        return yuekePlatforCommentmanagementMapper.selectPage(page, wrapper);
    }

    @Override
    public List<YuekePlatforCommentmanagement> selectAll() {
        return yuekePlatforCommentmanagementMapper.selectAll();
    }
}
