package com.yueke.platfor.platforweb.platforweb.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.platfor.YuekePlatforRvmaintenance;
import com.yueke.platfor.platforweb.platforweb.mapper.YuekePlatforRvmaintenanceMapper;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforRvmaintenanceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekePlatforRvmaintenanceServiceImpl extends ServiceImpl<YuekePlatforRvmaintenanceMapper, YuekePlatforRvmaintenance> implements YuekePlatforRvmaintenanceService {

    @Resource
    private YuekePlatforRvmaintenanceMapper yuekePlatforRvmaintenanceMapper;
    @Override
    public IPage<YuekePlatforRvmaintenance> selectAllByPage(Page<YuekePlatforRvmaintenance> page, QueryWrapper<YuekePlatforRvmaintenance> wrapper) {
        // 不进行 count sql 优化，解决 MP 无法自动优化 SQL 问题，这时候你需要自己查询 count 部分
        page.setOptimizeCountSql(false);
        // 当 total 为小于 0 或者设置 setSearchCount(false) 分页插件不会进行 count 查询
        // 要点!! 分页返回的对象与传入的对象是同一个
        return yuekePlatforRvmaintenanceMapper.selectPage(page, wrapper);
    }
}
