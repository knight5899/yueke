package com.yueke.platfor.platforweb.platforweb.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforCommentmanagementService extends IService<YuekePlatforCommentmanagement> {

    IPage<YuekePlatforCommentmanagement> selectAllByPage(Page<YuekePlatforCommentmanagement> page, QueryWrapper<YuekePlatforCommentmanagement> wrapper);
    List<YuekePlatforCommentmanagement> selectAll();
}
