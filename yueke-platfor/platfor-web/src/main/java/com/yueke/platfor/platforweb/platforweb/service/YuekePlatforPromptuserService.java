package com.yueke.platfor.platforweb.platforweb.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.platfor.YuekePlatforPromptuser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforPromptuserService extends IService<YuekePlatforPromptuser> {

}
