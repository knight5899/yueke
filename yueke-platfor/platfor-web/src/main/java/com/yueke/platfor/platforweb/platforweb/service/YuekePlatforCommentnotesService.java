package com.yueke.platfor.platforweb.platforweb.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentnotes;
import com.yueke.platfor.platforweb.param.FindCommentnotesForm;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforCommentnotesService extends IService<YuekePlatforCommentnotes> {
    List<YuekePlatforCommentnotes> findCommentnotes(FindCommentnotesForm form);

}
