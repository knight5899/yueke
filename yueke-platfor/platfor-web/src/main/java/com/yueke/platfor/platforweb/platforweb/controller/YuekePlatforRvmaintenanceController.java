package com.yueke.platfor.platforweb.platforweb.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;
import com.yueke.common.core.domain.platfor.YuekePlatforRvmaintenance;
import com.yueke.common.core.dto.ResultDto;
import com.yueke.platfor.platforweb.param.ApplyMaintenanceRvForm;
import com.yueke.platfor.platforweb.param.FindAllMaintenanceRvForm;
import com.yueke.platfor.platforweb.param.UpdateMaintenanceRvByIdForm;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforRvmaintenanceService;
import com.yueke.platfor.platforweb.platforweb.util.MyTimeUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@RestController
@RequestMapping("/platfor/rvmaintenance")
@Slf4j
public class YuekePlatforRvmaintenanceController {

    @Resource
    private YuekePlatforRvmaintenanceService yuekePlatforRvmaintenanceService;

    /**
     * 房车维修申请
     * @param form
     * @return
     */
    @PutMapping("applyMaintenanceRv")
    @ApiOperation("房车维修申请")
    public ResultDto applyMaintenanceRv(@RequestBody ApplyMaintenanceRvForm form){
        log.info("参数对象form:{}",form);
        if(form.getRvmaintenanceCompletiontime() == null) form.setRvmaintenanceCompletiontime(MyTimeUtil.addDays(3));
        YuekePlatforRvmaintenance yuekePlatforRvmaintenance = new YuekePlatforRvmaintenance();
        BeanUtils.copyProperties(form, yuekePlatforRvmaintenance);
        yuekePlatforRvmaintenance.setRvmaintenanceTime(new Date());
        yuekePlatforRvmaintenance.setRvmaintenanceProgress("等待");
        yuekePlatforRvmaintenance.setRvmaintenanceState("正常");
        boolean b = yuekePlatforRvmaintenanceService.save(yuekePlatforRvmaintenance);
        if(b)return ResultDto.success().setMsg("申请成功").setCode(200).setData(yuekePlatforRvmaintenance.getRvmaintenanceId());
        return ResultDto.success().setMsg("申请失败").setCode(400);
    }

    /**
     * 房车维修信息
     * @param form
     * @return
     */
    @GetMapping("findAllMaintenanceRv")
    @ApiOperation("房车维修信息")
    public ResultDto findAllMaintenanceRv(@RequestBody FindAllMaintenanceRvForm form){
        log.info("参数对象form:{}",form);
        QueryWrapper<YuekePlatforRvmaintenance> wrapper = new QueryWrapper<>();
        if(form.getRvmaintenanceId() != null && form.getRvmaintenanceId()+"" != "")
            wrapper.eq("rvmaintenance_id",form.getRvmaintenanceId());
        if(form.getRvmaintenanceRvfault() != null && form.getRvmaintenanceRvfault() != "")
            wrapper.like("rvmaintenance_rvFault",form.getRvmaintenanceRvfault());
        if(form.getRvmaintenanceStartTime() != null)
            wrapper.gt("rvmaintenance_time",form.getRvmaintenanceStartTime());
        if(form.getRvmaintenanceEndTime() != null)
            wrapper.lt("rvmaintenance_time",form.getRvmaintenanceEndTime());
        if(form.getRvmaintenanceCompletionStartTime() != null)
            wrapper.gt("rvmaintenance_completionTime",form.getRvmaintenanceCompletionStartTime());
        if(form.getRvmaintenanceCompletionEndTime() != null)
            wrapper.lt("rvmaintenance_completionTime",form.getRvmaintenanceCompletionEndTime());
        if(form.getRvmaintenanceProgress() != null && form.getRvmaintenanceProgress() != "")
            wrapper.like("rvmaintenance_progress",form.getRvmaintenanceProgress());
        if(form.getRvmaintenanceState() != null && form.getRvmaintenanceState() != "")
            wrapper.like("rvmaintenance_state",form.getRvmaintenanceState());
        Page<YuekePlatforRvmaintenance> page = new Page<>(form.getPageNum(),form.getPageSize());
        IPage<YuekePlatforRvmaintenance> byPage = yuekePlatforRvmaintenanceService.selectAllByPage(page, wrapper);
        return ResultDto.success().setMsg("查询成功").setCode(200).setData(byPage);
    }

    /**
     * 房车维修信息更新
     * @param form
     * @return
     */
    @PostMapping("updateMaintenanceRvById")
    @ApiOperation("房车维修信息更新")
    public ResultDto updateMaintenanceRvById(@RequestBody UpdateMaintenanceRvByIdForm form){
        log.info("参数对象form:{}",form);
        YuekePlatforRvmaintenance yuekePlatforRvmaintenance = new YuekePlatforRvmaintenance();
        BeanUtils.copyProperties(form, yuekePlatforRvmaintenance);
        System.out.println("yuekePlatforRvmaintenance:"+yuekePlatforRvmaintenance);
        boolean b = yuekePlatforRvmaintenanceService.updateById(yuekePlatforRvmaintenance);
        if(b)return ResultDto.success().setMsg("更新成功").setCode(200).setData(yuekePlatforRvmaintenance);
        return ResultDto.success().setMsg("更新失败").setCode(400);
    }




}

