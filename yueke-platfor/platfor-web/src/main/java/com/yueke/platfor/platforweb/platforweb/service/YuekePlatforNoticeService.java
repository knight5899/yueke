package com.yueke.platfor.platforweb.platforweb.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;
import com.yueke.common.core.domain.platfor.YuekePlatforNotice;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforNoticeService extends IService<YuekePlatforNotice> {
    IPage<YuekePlatforNotice> selectAllByPage(Page<YuekePlatforNotice> page, QueryWrapper<YuekePlatforNotice> wrapper);

}
