package com.yueke.platfor.platforweb.platforweb.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentmanagement;
import com.yueke.common.core.domain.platfor.YuekePlatforRvmaintenance;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
public interface YuekePlatforRvmaintenanceService extends IService<YuekePlatforRvmaintenance> {
    IPage<YuekePlatforRvmaintenance> selectAllByPage(Page<YuekePlatforRvmaintenance> page, QueryWrapper<YuekePlatforRvmaintenance> wrapper);

}
