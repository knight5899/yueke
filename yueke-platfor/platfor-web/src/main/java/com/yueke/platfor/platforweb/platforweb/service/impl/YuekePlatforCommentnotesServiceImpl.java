package com.yueke.platfor.platforweb.platforweb.service.impl;


import com.yueke.platfor.platforweb.param.FindCommentnotesForm;
import com.yueke.platfor.platforweb.platforweb.mapper.YuekePlatforCommentnotesMapper;
import com.yueke.platfor.platforweb.platforweb.service.YuekePlatforCommentnotesService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yueke.common.core.domain.platfor.YuekePlatforCommentnotes;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiyang
 * @since 2020-10-14
 */
@Service
public class YuekePlatforCommentnotesServiceImpl extends ServiceImpl<YuekePlatforCommentnotesMapper, YuekePlatforCommentnotes> implements YuekePlatforCommentnotesService {

    @Resource
    private YuekePlatforCommentnotesMapper yuekePlatforCommentnotesMapper;

    @Override
    public List<YuekePlatforCommentnotes> findCommentnotes(FindCommentnotesForm form) {
        return yuekePlatforCommentnotesMapper.findCommentnotes(form);
    }
}
